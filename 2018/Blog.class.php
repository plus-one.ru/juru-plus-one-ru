<?PHP

require_once('Widget.class.php');


class Blog extends Widget
{
    private $salt = 'simpla';

    private $postsMatrix;

    private $numItemsOnPage;


    /**
     *
     * Конструктор
     *
     */
    function Blog(&$parent)
    {

        Widget::Widget($parent);
        $this->postMatrix = array(
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/2' => array('settings' => array('block' => '1/2', 'start' => 0, 'limit' => 2), 'items' => null)),
            array('1/4' => array('settings' => array('block' => '1/4', 'start' => 0, 'limit' => 4), 'items' => null)),
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/3' => array('settings' => array('block' => '1/3', 'start' => 0, 'limit' => 3), 'items' => null)),
            array('1/2' => array('settings' => array('block' => '1/2', 'start' => 0, 'limit' => 2), 'items' => null)),
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/4' => array('settings' => array('block' => '1/4', 'start' => 0, 'limit' => 4), 'items' => null)),
            array('1/3' => array('settings' => array('block' => '1/3', 'start' => 0, 'limit' => 3), 'items' => null)),
        );
        $this->postsMatrix = new stdClass();
        $this->numItemsOnPage = 3;
        $this->numAuthorsOnPage = 12;
    }

    /**
     *
     * Отображение
     *
     */
    function fetch()
    {
        $mode = $this->url_filtered_param('mode');
        $nomination = $this->url_filtered_param('nomination');

        if ($mode == "login") {
            return $this->actionLogin();
        }
        elseif ($mode == "logout"){
            return $this->actionLogout();
        }
        elseif ($mode == "nominations"){
            return $this->actionNominations();
        }
        elseif ($mode == "evaluation" && $nomination){
            return $this->actionNomination($nomination);
        }
        elseif ($mode == "freeze" && $nomination){
            return $this->actionFreeze($nomination);
        }
        else {

            if (is_null($this->user)){
                return $this->actionLogin();
            }
            else{
                return $this->actionNominations();
            }
        }
    }

    function actionLogin()
    {
        // Если запостили email и пароль, попытаемся залогинить
        if (isset($_POST['log-in']) && isset($_POST['pas-d'])) {
            // Берем "голые" данные, но это безопасно
            $email = substr($_POST['log-in'], 0, 100);
            $password = substr($_POST['pas-d'], 0, 25);
            $encpassword = md5($password . $this->salt);

            // Выбираем из базы пользователя с таки логином и паролем
            $query = sql_placeholder('SELECT * FROM users WHERE login=? AND password=? limit 1', $email, $encpassword);
            $this->db->query($query);
            $user = $this->db->result();

            if (empty($user)) {
                // Если такого нет
                $error = 'Неверный email или пароль';
            }
            elseif ($user->enabled == 0) {
                // Если пользователь не активен
                $error = 'Ваша учетная запись отключена. Пожалуйста, обратитесь к администратору';
            }

            if (!empty($error)) {
                // В случае ошибки предлагаем опять форму логина
                $this->smarty->assign('error_login', $error);
                $this->smarty->assign('email', $email);
                $this->smarty->assign('password', $password);
                $this->body = $this->smarty->fetch('login.tpl');
                return $this->body;
            } else {
                // Если все хорошо - залогиниваем, и переходим на главную
                $_SESSION['user_email'] = $email;
                $_SESSION['user_password'] = $encpassword;


                header("Location: http://$this->root_url/");
                exit();
            }
        }
        else{
            $this->body = $this->smarty->fetch('login.tpl');
            $this->section->meta_title = 'Вход на сайт';
            return $this->body;
        }
    }

    function actionLogout()
    {
        unset($_SESSION['user_email']);
        unset($_SESSION['user_password']);
        unset($_SESSION['group']);
        header("Location: http://$this->root_url/");
        exit();
    }


    function actionNominations()
    {
        $nominations = array();

        // Id судьи
        $judgeId = $this->user->id;

        $query = sql_placeholder("SELECT * FROM blogtags WHERE enabled = 1 AND isshow = 1");
        $this->db->query($query);
        $nominations = $this->db->results();

        foreach ($nominations AS $k=>$v){
            $nominations[$k]->assessmentNominees = 0;
            $query = sql_placeholder("SELECT id FROM blogposts WHERE enabled = 1 AND tags = ?", $v->id);
            $this->db->query($query);
            $nomenees = $this->db->results();

            foreach ($nomenees AS $kn=>$n){
                $query = sql_placeholder("SELECT id FROM assessments WHERE nomenee_id = ? AND judge_id=?", $n->id, $judgeId);
                $this->db->query($query);
                $assessmentNomenee = $this->db->result();

                if ($assessmentNomenee){
                    $nominations[$k]->assessmentNominees += 1;
                }
            }
            $nominations[$k]->totalNominees = count($nomenees);
        }

        $this->smarty->assign('nominations', $nominations);

        $this->body = $this->smarty->fetch('nominations.tpl');

        $this->title = 'Выберите номинацию для судейства';

        return $this->body;
    }

    function actionNomination($nominationUrl)
    {
        // флаг определяющий можно ли закрыть голосование (все оценки расставлены по всем номинмнтам) в текущей номинации
        // 0 - можно закрывать
        // 1 - нельзя закрывать
        $checkFreeze = 0;

        // Id судьи
        $judgeId = $this->user->id;

        $query = sql_placeholder("SELECT * FROM blogtags WHERE enabled = 1 AND isshow = 1 AND url = ?", $nominationUrl);
        $this->db->query($query);
        $nomination = $this->db->result();

        // усли случился пост значит нам прилетел результат голосования
        if ($_POST){
            $this->saveAssessment();
        }

        $query = sql_placeholder("SELECT id, name, header, body FROM blogposts WHERE enabled = 1 AND tags = ?", $nomination->id);
        $this->db->query($query);
        $nominees = $this->db->results();

        foreach ($nominees AS $k=>$n){

            $query = sql_placeholder("SELECT group_1 AS g1, group_2 AS g2, group_3 AS g3, group_4 AS g4, frozen FROM assessments WHERE nomenee_id = ? AND judge_id = ?",
                $n->id, $judgeId);
            $this->db->query($query);
            $asnt = $this->db->result();

            $averageScore = ($asnt->g1 + $asnt->g2 + $asnt->g3 + $asnt->g4) / 4;
            if ($averageScore == 0){
                $checkFreeze = 1;
            }
            $nominees[$k]->averageScore = $averageScore;
            $nominees[$k]->group_1 = $asnt->g1;
            $nominees[$k]->group_2 = $asnt->g2;
            $nominees[$k]->group_3 = $asnt->g3;
            $nominees[$k]->group_4 = $asnt->g4;
            $nominees[$k]->frozen = $asnt->frozen;

        }



        $this->smarty->assign('checkFreeze', $checkFreeze);
        $this->smarty->assign('nomination', $nomination);
        $this->smarty->assign('nominees', $nominees);

        $this->body = $this->smarty->fetch('nomination.tpl');

        // Устанавливаем метатеги для списка (если он вызван как голый модуль)
        $this->title = $nomination->name;

        return $this->body;
    }


    function actionFreeze($nominationUrl)
    {
        // Id судьи
        $judgeId = $this->user->id;

        $query = sql_placeholder("SELECT * FROM blogtags WHERE enabled = 1 AND isshow = 1 AND url = ?", $nominationUrl);
        $this->db->query($query);
        $nomination = $this->db->result();

        $query = sql_placeholder("SELECT id, name, header, body FROM blogposts WHERE enabled = 1 AND tags = ?", $nomination->id);
        $this->db->query($query);
        $nominees = $this->db->results();

        foreach ($nominees AS $k=>$n){

            $query = sql_placeholder("UPDATE assessments SET frozen = 1 WHERE nomenee_id = ? AND judge_id = ?",
                $n->id, $judgeId);
            $this->db->query($query);
        }

        header("Location: http://$this->root_url/");
        exit();
    }


    function saveAssessment()
    {
        $judgeId = $this->user->id;

        $nomineeId = $_POST['nomineeid'];
        $nominationid = $_POST['nominationid'];
        $group1 = 0;
        if (isset($_POST['group1_' . $nomineeId])){
            $group1 = $_POST['group1_' . $nomineeId];
        }

        $group2 = 0;
        if (isset($_POST['group2_' . $nomineeId])){
            $group2 = $_POST['group2_' . $nomineeId];
        }

        $group3 = 0;
        if (isset($_POST['group3_' . $nomineeId])){
            $group3 = $_POST['group3_' . $nomineeId];
        }

        $group4 = 0;
        if (isset($_POST['group4_' . $nomineeId])){
            $group4 = $_POST['group4_' . $nomineeId];
        }

        $query = sql_placeholder("SELECT COUNT(id) AS cnt, id, frozen FROM assessments WHERE nomenee_id = ? AND judge_id = ? GROUP BY id", $nomineeId, $judgeId);
        $this->db->query($query);
        $checkAssessment = $this->db->result();

        // проверяем не заморожена ли оценка (если заморожена - то оценивать ее уже нельзя) и есть ли уже оценка
        if (!$checkAssessment){
            // оценка еще нет - надо добавить запись
            $query = sql_placeholder('INSERT IGNORE INTO assessments SET nomenee_id=?, nomination_id = ?, judge_id=?, group_1=?, group_2=?, group_3=?, group_4=?, created=?',
                $nomineeId, $nominationid, $judgeId, $group1, $group2, $group3, $group4, time()
            );
            $this->db->query($query);
        }
        else{
            if ($checkAssessment->frozen == 0){

                if ($checkAssessment->cnt == "1"){
                    // оценка уже есть - надо обновить запись
                    $query = sql_placeholder('UPDATE assessments SET group_1=?, group_2=?, group_3=?, group_4=? WHERE id=? ',
                        $group1, $group2, $group3, $group4, $checkAssessment->id
                    );
                    $this->db->query($query);
                }
            }
        }
    }
}