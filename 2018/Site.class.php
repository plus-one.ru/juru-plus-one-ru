<?PHP
// Как и все классы, наследуется от класса widget
require_once('Widget.class.php');
require_once('Blog.class.php');

class Site extends Widget
{
    /** @var Blog main */
    var $main; // Центральный блок (класс). В данном случае он может быть разных классов, в зависимости от раздела
    var $categories; // Категории товаров
    var $articles_count = 3; // Количество свежих статей
    var $news_count = 3; // Количество свежих новостей
    var $section; // текущий раздел
    var $current_url = "";

    /**
     *
     * Конструктор
     *
     */
    function Site(&$parent)
    {
        Widget::Widget($parent);

        // Пока не знаем какого класса центральный блок, он будет базового класса
        $this->main = new Widget($this);


        // Необходимо определить что выводить в центральном блоке.
        // Это может быть указано в качестве конкретного раздела сайта (section)
        // или в качестве указания модуля, который следует вывести
        // В первом случае мы узнаем из базы какого модуля нужный раздел
        // и создаем соотвествующий класс (например статическая страница или лента новостей)
        // а во втором - и узнавать ничего не надо, модуль по сути и есть класс
        // (ну почти, нужно только глянуть в таблице modules название класса для данного модуля, обычно оно совпадает)

        // итак, url текущего раздела (может быть не задан)
        $section_url = $this->url_filtered_param('section');

        // модуль (тоже может быть не задан)
        $module = $this->url_filtered_param('module');

        // если ничего не задано, текущим разделом будет раздел, заданный в настройках как главная страница
        if (empty($section_url) && empty($module)) {
            $section_url = $this->settings->main_section;

        }

        // если url раздела задан,
        if (!empty($section_url)) {
            // выбираем из базы этот раздел
            $query = sql_placeholder("SELECT sections.section_id, modules.module_id, modules.class, sections.url, sections.body, sections.header, sections.module_id, sections.meta_title, sections.meta_description, sections.meta_keywords  FROM sections LEFT JOIN modules ON sections.module_id=modules.module_id WHERE sections.url=? limit 1", $section_url);
            $this->db->query($query);
            $this->section = $this->db->result();
            $this->smarty->assign('section', $this->section);
        }

        // Если раздел с таким url действительно существует,
        if (!empty($this->section)) {
            $this->current_url = $this->section->url;
            // создадим класс этого раздела
            $class = $this->section->class;

            include_once("$class.class.php");

            if (class_exists($class)) {
                $_GET['section'] = $this->section->url;

                $this->main = new $class($this);
            } // возвращение false приводит к отображению 404 ошибки. Кстати это работает в любом модуле
            else {
                return false;
            }
        } // Если задан модуль, аналогично создаем нужный класс
        elseif (!empty($module)) {

            $query = sql_placeholder("SELECT class,url FROM modules WHERE modules.class=? LIMIT 1", $module);
            $this->db->query($query);
            $module = $this->db->result();
            $this->current_url = $module->url;

            if (!empty($module->class)) {
                $class = $module->class;
                include_once("$class.class.php");
                if (class_exists($class)) {
                    $this->main = new $class($this);
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    /**
     *
     * Отображение
     *
     */
    function fetch()
    {

        // Создаем основной блок страницы
        if (!$this->main->fetch()) {
            return false;
        }
        // Если у main установлен флаг single, значит страница состоит только из main, не обромляем ее ничем больше
        if (isset($this->main->single) && $this->main->single) {
            $this->body = $this->main->body;
            return $this->body;
        } else {
            $content = $this->main->body;

            //  Устанавливаем мета-теги, указанные в главном блоке
            $this->title = isset($this->section->meta_title) ? $this->section->meta_title : $this->main->title;
            $this->ogTitle = isset($this->section->meta_title) ? $this->section->meta_title : $this->main->title;
            $this->keywords = isset($this->section->meta_keywords) ? $this->section->meta_keywords : $this->main->keywords;
            $this->description = isset($this->section->meta_description) ? $this->section->meta_description : $this->main->description;

            if (!$this->user){
                $this->title = 'Авторизация';
            }
        }

        $this->smarty->assign('site_name', $this->settings->site_name);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('keywords', $this->keywords);
        $this->smarty->assign('description', $this->description);
        $this->smarty->assign('content', $content);
        $this->smarty->assign('current_url', $this->current_url);

        $this->smarty->assign('ogTitle', $this->ogTitle);
        $this->smarty->assign('ogImage', "og_1920-01.png");
        // Создаем текущую страницу сайта
        $this->body = $this->smarty->fetch('index.tpl');

        return $this->body;
    }
}
