<!DOCTYPE html>
<html dir="ltr" class="ltr" lang="ru">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="robots" content="all" />
    <title>{$title|escape}</title>
    <meta name="description" content="{$description|escape}" />
    <meta name="keywords" content="{$keywords|escape}" />
    <meta property="og:title" content="{$ogTitle|escape}" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{$description|escape}" />
    <meta property="og:image" content="http://{$root_url}/{$ogImage}" />
    <meta property="og:site_name" content="{$ogTitle|escape}" />
    <meta property="og:url" content="http://{$root_url}/{$ogUrl}" />
    <meta name="twitter:title" content="{$ogTitle|escape}" />
    <meta name="twitter:description" content="{$description|escape}" />
    <meta name="twitter:url" content="http://{$root_url}/{$ogUrl}" />
    <meta name="twitter:image" content="http://{$root_url}/{$ogImage}" />
    <meta name="twitter:image:alt" content="{$ogTitle|escape}" />
    <meta name="twitter:site" content="{$ogTitle|escape}" />
    <link rel="image_src" href="http://{$root_url}/{$ogImage}">
    <base href="http://{$root_url}/">
    <link media="all" rel="stylesheet" href="http://{$root_url}/design/{$settings->theme|escape}/css/main.css">
</head>
<body>
    <div id="wrapper" class="login-page">
        <header id="header">
            <div class="container">
                <div class="logo">
                    <a href="./">
                        <img src="http://{$root_url}/design/{$settings->theme|escape}/images/logo.svg" alt="Plus 1" width="130" height="68" />
                    </a>
                </div>
            </div>
        </header>
        <main id="main" role="main">
            {$content}
        </main>
    </div>
    <div class="page-bg">
        <img src="http://{$root_url}/design/{$settings->theme|escape}/images/page-bg.svg" alt="image description" />
    </div>
    <script src="http://{$root_url}/design/{$settings->theme|escape}/js/jquery-3.2.1.min.js" defer></script>
    <script src="http://{$root_url}/design/{$settings->theme|escape}/js/jquery.main.js" defer></script>
</body>
</html>

