<section class="section">
    <div class="container">
        <h1 class="section_title text-uppercase section_title--with-border">Выберите номинацию <br>для судейства</h1>
        <ul class="nominee-list">
            {foreach name=nominations from=$nominations item=a}
            <li {if $a->assessmentNominees == $a->totalNominees}class="full"{/if}>
                <a href="/evaluation/{$a->url}/">
                    <div>{$a->name|escape}</div>
                    <div>
                        <div class="nominee-list_num">
                            {$a->assessmentNominees}/{$a->totalNominees}
                        </div>
                        <img class="ico-check" src="http://{$root_url}/design/{$settings->theme|escape}/images/check.svg" alt="" />
                    </div>
                </a>
            </li>
            {/foreach}
        </ul>
        <div class="text-center section__text-box">
            <p>Голосование жюри продлится до 19 апреля (15.00)</p>
        </div>
    </div>
</section>
<section class="section">
    <div class="container">
        <form action="#" class="jury-form">
            <h1 class="section_title text-uppercase">
                {$user->name|escape}
            </h1>
            <a href="/logout" class="btn">Выйти</a>
        </form>
    </div>
</section>