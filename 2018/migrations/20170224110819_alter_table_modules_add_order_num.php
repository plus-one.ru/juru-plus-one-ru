<?php

use Phinx\Migration\AbstractMigration;

class AlterTableModulesAddOrderNum extends AbstractMigration
{
    private $tablename = 'modules';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('order_num', 'integer', ['null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('order_num')
            ->save();
    }
}
