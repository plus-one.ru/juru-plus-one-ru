<?php

use Phinx\Migration\AbstractMigration;

class AlterTableUsersAdminAddColumnsName extends AbstractMigration
{
    private $tablename = 'users_admin';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('name', 'string', ['limit'=> 255, 'null' => false, 'default' => ''])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('name')
            ->save();
    }
}
