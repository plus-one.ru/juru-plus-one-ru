<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

{if $Error}
    <div class="row">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {$Error}
        </div>
    </div>
{/if}

<form name="blogpost" id="blogpost_form" method="post" action="blogpost/save/{$Token}/{$Item->id}" class="dropzone" enctype="multipart/form-data">
    <input type="hidden" name="item_id" id="posttag_id" value="{$Item->id}">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="well">

                                <div class="form-group">
                                    <label id="name_label" {if !$Item->name}class="text-danger"{/if}>Название
                                        проекта</label>
                                    <input id="name" name="name" type="text" value='{$Item->name|escape}'
                                           class="form-control">
                                </div>

                                <div class="form-group">
                                    <label id="header_label"
                                           {if !$Item->name}class="text-danger"{/if}>Организация</label>
                                    <input id="header" name="header" type="text" value='{$Item->header|escape}'
                                           class="form-control">
                                </div>

                                <div class="form-group">
                                <h5>Номинации</h5>
                                <select id="post_tag_first" class="selectpicker form-control" data-max-options="1" multiple data-live-search="true" name="tags" title="-- Укажите тег статьи для главной страницы --">
                                    {foreach item=tag from=$tags name=tag}
                                        {if $tag->url != 'main'}
                                            <option value="{$tag->id}" {if $tag->id == $Item->tags}selected{/if}>{$tag->name|escape}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                    </div>

                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input id="enabled" type="checkbox" name="enabled" value="1"
                                                   {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12" style="min-height: 20px;">
                            <!-- /-->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="well">
                                <div class="form-group">
                                    {*<div class="stk-editor" id="setka-editor"></div>*}
                                    <label>Содержимое анкеты</label>
                                    <textarea rows="20" name="body" id="body"
                                              class="form-control fulleditor">{$Item->body}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="type_visual_block" value="post">
    <input type="hidden" name="item_id" id="item_id" value="{$Item->id}">
</form>


<div class="modal fade bs-example-modal-sm" tabindex="-1" id="modalerror" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">Ошибка. </h4>
            </div>
            <div class="modal-body">
                <h5 id="errormessage">...</h5>
            </div>
        </div>
    </div>
</div>

{include file='tinymce_init.tpl'}