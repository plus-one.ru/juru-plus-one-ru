<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            <small>{$title|escape}</small>
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                &nbsp;
                <div class="pull-right">
                    <div class="btn-group">
                        <a class="btn btn-primary btn-xs" type="button" href="index.php?section=Judge&token={$Token}">
                            <i class="fa fa-check"></i> Добавить
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                {if $Items}
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th><!-- /--></th>
                                <th>Имя</th>
                                <th>Login</th>
                                <th style="text-align: center">Общество</th>
                                <th style="text-align: center">Экология</th>
                                <th style="text-align: center">Экономика</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach item=item from=$Items name=item}
                                <tr>
                                    <td nowrap>
                                        <a href="index.php{$item->enable_get}">
                                            <i class="fa fa-toggle-{if $item->enabled}on{else}off{/if} fa-1x"></i>
                                        </a>
                                    </td>
                                    <td width="35%">
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            <a href="index.php{$item->edit_get}">{$item->name|escape}</a>
                                        </p>
                                    </td>
                                    <td width="35%">
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            <a href="index.php{$item->edit_get}">{$item->login|escape}</a>
                                        </p>
                                    </td>
                                    <td width="10%" style="text-align: center">
                                        <p class="{$item->communityClass}">
                                            {if $item->communityClass == "text-success"}
                                                <i class="fa fa-check-circle"></i>
                                            {else}
                                                <i class="fa fa-ban"></i>
                                            {/if}
                                            {$item->communityJudgeTotalCount}/{$item->communityNomeneeTotalCount}
                                        </p>
                                    </td>
                                    <td width="10%" style="text-align: center">
                                        <p class="{$item->ecologyClass}">
                                            {if $item->ecologyClass == "text-success"}
                                                <i class="fa fa-check-circle"></i>
                                            {else}
                                                <i class="fa fa-ban"></i>
                                            {/if}
                                            {$item->ecologyJudgeTotalCount}/{$item->ecologyNomeneeTotalCount}
                                        </p>
                                    </td>
                                    <td width="10%" style="text-align: center">
                                        <p class="{$item->economyClass}">
                                            {if $item->economyClass == "text-success"}
                                                <i class="fa fa-check-circle"></i>
                                            {else}
                                                <i class="fa fa-ban"></i>
                                            {/if}
                                            {$item->economyJudgeTotalCount}/{$item->economyNomeneeTotalCount}
                                        </p>
                                    </td>

                                    <td nowrap>
                                        <a href="index.php{$item->edit_get}" class="btn btn-success btn-xs"
                                           type="button">
                                            <i class="fa fa-pencil fa-1x"></i>
                                        </a>
                                        <a href="index.php{$item->delete_get}" class="btn btn-danger btn-xs"
                                           type="button"
                                           onclick='if(!confirm("Удалить?")) return false;'>
                                            <i class="fa fa-times-circle fa-1x"></i>
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>

                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p align="center">{$PagesNavigation}</p>
                        </div>
                    </div>
                {else}
                    <p>Еще нет ни одной статьи. <a href="index.php?section=Article&token={$Token}">Добавить статью?</a>
                    </p>
                {/if}
            </div>
        </div>
    </div>
</div>