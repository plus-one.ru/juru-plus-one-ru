{if $Item->photos}
    <div id="imagesList">
        <div class="row h1">
            <div class="pull-right">
                <div class="btn-group" style="margin-right: 30px;">
                    <button class="btn btn-success saveImageTitle" galleryid="{$Item->galleryId}" photoId="{$photo->id}" type="button">
                        <i class="fa fa-save"></i> Сохранить подписи
                    </button>
                </div>
            </div>
        </div>

    {foreach from=$Item->photos item=photo name=photo}
        <div class="col-lg-6">
            <div class="well">
                <div class="row">
                    <div class="col-lg-3">
                        <div style="min-height: 200px;">
                            <img src="../files/photogallerys/{$photo->filename}" style="width: 143px;"/>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="form-group">
                            <input photoId="{$photo->id}" name="image_name" type="text" value='{$photo->image_name|escape}' id="{$photo->id}" placeholder="Описание фотографии" class="form-control">
                        </div>
                        <div class="form-group">
                            <input photoId="{$photo->id}" name="image_author" type="text" value='{$photo->image_author|escape}' id="{$photo->id}" placeholder="Источник фотографии" class="form-control">
                        </div>

                        <div class="pull-right">
                            <div class="btn-group">

                                <button class="btn btn-danger deletePhoto" galleryid="{$Item->galleryId}" photoId="{$photo->id}" type="button">
                                    <i class="fa fa-times-circle fa-1x"></i> Удалить изображение
                                </button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    {/foreach}
    </div>
{else}
    <div class="well">
        Нет фото. Воспользуйтесь формой для загрузки изображений.
    </div>
{/if}