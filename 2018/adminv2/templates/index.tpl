<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="http://{$root_url}/adminv2/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{$Title}</title>
    <link href="bootstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!--<link href="bootstrap/dist/css/timeline.css" rel="stylesheet">/-->

    <link href="bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="bootstrap/dist/css/dropzone.css" rel="stylesheet">
    <link href="bootstrap/dist/css/bootstrap-select.css" rel="stylesheet">

    {*<link href="bootstrap/dist/css/bootstrap-datepicker.css" rel="stylesheet">*}
    <link href="bootstrap/dist/css/jquery.datetimepicker.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="bootstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- DataTables Responsive CSS -->
    <link href="bootstrap/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    {* setka *}
    <link href="{$contentEditorCss}" rel="stylesheet">
    <link href="{$themeFileCss}" rel="stylesheet">



    <script src="bootstrap/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bootstrap/dist/js/jquery-ui-1.10.3.custom.js"></script>
    <script src="bootstrap/dist/js/sortable.js"></script>

    <script src="bootstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bootstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/r/bs/dt-1.10.9/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.12/sorting/date-de.js"></script>
    <script src="bootstrap/dist/js/jquery.datetimepicker.min.js"></script>

    <script src="{$contentEditorJs}"></script>

</head>
	<body>
		<div id="wrapper">
			<!-- Navigation -->
			<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
				{include file="include_topnav.tpl"}
				<div class="navbar-default sidebar" role="navigation" style="width: 170px;">
					<div class="sidebar-nav navbar-collapse">
						{include file="include/include_leftmenu.tpl"}
					</div>
				</div>
			</nav>
			<div id="page-wrapper" style="margin: 0 0 0 170px">
				{$Body}
			</div>
		</div>



        <script src="bootstrap/dist/js/dropzone.js"></script>
        <script src="bootstrap/dist/js/bootstrap-select.js"></script>
        <script src="bootstrap/dist/js/sb-admin-2.js"></script>

    
	</body>
</html>