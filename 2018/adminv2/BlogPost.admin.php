<?PHP
require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once('BlogWriters.admin.php');
require_once('BlogTags.admin.php');
require_once('PostTags.admin.php');
require_once('BlogPosts.admin.php');
require_once('UnitMeasures.admin.php');
require_once('Specprojects.admin.php');
require_once('Conferences.admin.php');

/**
 * Class BlogPost
 */
class BlogPost extends Widget
{
    /** @var   */
    public $item;
    /** @var string  */
    private $tableName = 'blogposts';
    /** @var string  */
    public $uploaddir = '../files/blogposts/';
    /** @var string  */
    public $large_image_width = "900";
    /** @var string  */
    public $large_image_height = "600";

    /** @var  stdClass */
    private $setkaConfig;

    public $sizeImage_1_1 = array('x' => 1141, 'y' => 505);
    public $sizeImage_1_2 = array('x' => 559, 'y' => 365);
    public $sizeImage_1_3 = array('x' => 363, 'y' => 293);
    public $sizeImage_1_4 = array('x' => 615, 'y' => 410);

    function BlogPost(&$parent){
        Widget::Widget($parent);

        $this->prepare();
    }

    function setData($post, $itemId){
        $this->item->id = $itemId;
        $this->item->name = $post['name'];
        $this->item->header = $post['header'];
        $this->item->body = $post['body'];

        $this->item->tags = $post['tags'];

        $this->item->enabled = 0;
        if(isset($_POST['enabled']) && $_POST['enabled']==1) {
            $this->item->enabled = 1;
        }
    }

    function checkRequiredFields(){
        switch (intval($this->item->type_post)) {
            case 1:
            case 2:
            case 3:
                // проверка на обычный пост
                if (empty($this->item->name) || empty($this->item->header)){
                    $this->error_msg = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                }
        }
    }

    function checkUrl($itemId){
        $query = sql_placeholder('SELECT COUNT(*) AS count FROM ' . $this->tableName . ' WHERE url=? AND id!=?', $this->item->url, $itemId);
        $this->db->query($query);
        $res = $this->db->result();

        if($res->count>0){
            $this->error_msg = 'Пост не сохранен. Запись с таким URL уже существует. Укажите другой.';
        }
    }

    function prepare(){
        $itemId = intval($this->param('item_id'));

        if ($_POST){
            $this->check_token();

            // создаем объект из переданных данных
            $this->setData($_POST, $itemId);

            // проверка на обязательные поля
            $this->checkRequiredFields();

            //Не допустить одинаковые URL статей
            $this->checkUrl($itemId);

            if (empty($this->error_msg)) {
                if(empty($itemId)) {
                    $this->item->created = date('Y-m-d H:i:s', time());
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $itemId = $this->add_article();
                    if (is_null($itemId)){
                        $this->error_msg = 'Пост не сохранен. Ошибка при сохранении записи.';
                    }
                }
                else{
                    $this->item->modified = date('Y-m-d H:i:s', time());
                    if (is_null($this->update_article($itemId))){
                        $this->error_msg = 'Пост не сохранен. Ошибка при сохранении записи.';
                    }
                }


                // добавляем данные для аналитики
                if ($_POST['value_to_yeats']){
                    // записываем данные аналитики в бд
                    $this->setWritersData($itemId, $this->item->writers, $_POST);
                }

                // загрузка картинки
                $this->add_fotos($itemId);

                header("Location: /adminv2/blogpost/");

            }
        }
        elseif (!empty($itemId)){
            $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i') AS date_created FROM " . $this->tableName . " WHERE id=?", $itemId);
            $this->db->query($query);
            $this->item = $this->db->result();
        }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новая номинант';
            $this->item->type_post = 1;
            $this->item->date_created = date('d.m.Y H') . ":00";
		}
		else{
			$this->title = 'Изменение номинанта: ' . $this->item->name;
		}

        $bt = new BlogTags();
        $tags = $bt->getTags();

        $this->smarty->assign('title', $this->title);

        $this->smarty->assign('tags', $tags);

        $this->smarty->assign('Item', $this->item);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);

        $this->smarty->assign('root_url', $this->root_url);

        $this->body = $this->smarty->fetch('blogpost/blogpost.tpl');


	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);

        if ($this->db->query($query)){
            $itemId = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $itemId);
            $this->db->query($query);

            return $itemId;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($itemId){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $itemId);

        if ($this->db->query($query)){
            return $itemId;
        }
        else{
            return null;
        }
    }

    /**
     * сохранение связей между постом и тегами-разделами
     *
     * @param array $tags
     * @param int $itemId
     */
    function setRelationsTags($tags=array(), $itemId=0){
        if (!empty($tags) && $itemId!=0){
            // чистим предыдущие связи
            $query = sql_placeholder('DELETE FROM relations_post_tags WHERE post_id=?', $itemId);
            $this->db->query($query);
            foreach ($tags AS $tag){
                $query = sql_placeholder('INSERT INTO relations_post_tags SET tag_id=?, post_id=?', $tag, $itemId);
                $this->db->query($query);
            }
        }
        elseif (empty($tags) && $itemId!=0){
            // если к нам прилетел пустой массив с тегами первого уровня (рубриками)
            // это значит что пост должен быть удален отовсюду.
            // и отображаться только в блоге автора материала
            $query = sql_placeholder('DELETE FROM relations_post_tags WHERE post_id=?', $itemId);
            $this->db->query($query);
        }
    }

    function setWritersData($postId = 0, $writerId = 0, $data = array()){
        if ($postId != 0 && $writerId != 0 && !empty($data)){
            if (isset($data['value_to_yeats'])){

                $query = sql_placeholder('DELETE FROM blogwriters_data WHERE post_id=? AND writer_id=?', $postId, $writerId);
                $this->db->query($query);

                foreach ($data['value_to_yeats'] AS $key=>$value){
                    if (!empty($value)){
                        $year = $data['years'][$key];

                        $value = str_replace(",", ".", $value);

                        $query = sql_placeholder('INSERT INTO blogwriters_data SET post_id=?, writer_id=?, years=?, value_to_yeats=?',
                            $postId, $writerId, $year, $value);
                        $this->db->query($query);
                    }
                }
            }
            return false;
        }
        return false;
    }

    /**
     * сохранение связей между постом и тегами самой статьи (облаком тегов)
     *
     * @param array $tags
     * @param int $itemId
     */
    function setRelationsPostItemTags($tags=array(), $itemId=0){
        if (!empty($tags) && $itemId!=0){
            // чистим предыдущие связи
            $query = sql_placeholder('DELETE FROM relations_postitem_tags WHERE post_id=?', $itemId);
            $this->db->query($query);

            if (is_array($tags)){
                foreach ($tags AS $posttagId){
                    $query = sql_placeholder('INSERT INTO relations_postitem_tags SET posttag_id=?, post_id=?', $posttagId, $itemId);
                    echo $query . "<br/>";
                    $this->db->query($query);
                }
            }
            elseif($tags != 0){
                $query = sql_placeholder('INSERT INTO relations_postitem_tags SET posttag_id=?, post_id=?', $tags, $itemId);
                echo $query . "<br/>";
                $this->db->query($query);
            }
        }
    }

    /**
     * добавление партнерского поста
     *
     * @param int $postId
     * @param array $partnerPosts
     * @return null
     */
    function addRelatedPartnersPosts($postId = 0, $partnerPosts = array()){

        if ($postId != 0 && !empty($partnerPosts)){
            foreach ($partnerPosts AS $key=>$post){
                // проверим, нет ли такого партнерского материала, у этой статьи с такой же ссылкой
                $query = sql_placeholder('SELECT id FROM partners_posts_links WHERE post_id=? AND link=?',
                    $postId, $post['link']);
                $this->db->query($query);
                $availablePartnerPost = $this->db->result();

                if (!$availablePartnerPost) {
                    $post['post_id'] = $postId;
                    if(isset($_FILES)){
                        $post['image'] = '';
                    }

                    $query = sql_placeholder('INSERT INTO partners_posts_links SET ?%', $post);
                    $this->db->query($query);
                    $partnerPostId = $this->db->insert_id();

                    $this->addPartnerPostPhoto($partnerPostId, $key);
                }
                else{
                    $query = sql_placeholder('UPDATE partners_posts_links SET ?% WHERE id=?', $post, $availablePartnerPost->id);
                    $this->db->query($query);

                    $this->addPartnerPostPhoto($availablePartnerPost->id, $key);
                }

            }
        }
        else{
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $largeuploadfile = $itemId.".jpg";

        $image_1_1 = $itemId . '-1_1.jpg';
        $image_1_1_c = $itemId . '-1_1_c.jpg';
        $image_1_2 = $itemId . '-1_2.jpg';
        $image_1_3 = $itemId . '-1_3.jpg';
        $image_1_4 = $itemId . '-1_4.jpg';
        $image_rss = $itemId . '-rss.jpg';

        if(isset($_FILES['image_1_1']) && !empty($_FILES['image_1_1']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_1']['tmp_name'], $this->uploaddir.$image_1_1)){
                echo "a";
                echo $this->uploaddir.$image_1_1;
                $this->error_msg = 'Ошибка при загрузке файла 1/1';
            }
            else{
                echo "b";
                $this->db->query("UPDATE {$this->tableName} SET image_1_1='$image_1_1' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_1_c']) && !empty($_FILES['image_1_1_c']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_1_c']['tmp_name'], $this->uploaddir.$image_1_1_c)){
                $this->error_msg = 'Ошибка при загрузке файла 1/1 цитата';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_1_c='$image_1_1_c' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_2']) && !empty($_FILES['image_1_2']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_2']['tmp_name'], $this->uploaddir.$image_1_2)){
                $this->error_msg = 'Ошибка при загрузке файла 1/1';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_2='$image_1_2' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_3']) && !empty($_FILES['image_1_3']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_3']['tmp_name'], $this->uploaddir.$image_1_3)){
                $this->error_msg = 'Ошибка при загрузке файла 1/3';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_3='$image_1_3' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_4']) && !empty($_FILES['image_1_4']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_4']['tmp_name'], $this->uploaddir.$image_1_4)){
                $this->error_msg = 'Ошибка при загрузке файла 1/4';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_4='$image_1_4' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_rss']) && !empty($_FILES['image_rss']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_rss']['tmp_name'], $this->uploaddir.$image_rss)){
                $this->error_msg = 'Ошибка при загрузке файла RSS';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_rss='$image_rss' WHERE id={$itemId}");
                $result = true;
            }
        }

        return $result;
    }

    /**
     * добавление фото к партнерскому посту
     *
     * @param $partnerPostId
     * @param $key
     * @return bool
     */
    function addPartnerPostPhoto($partnerPostId, $key){
        $result = false;

        $imageName = 'pi_' . $partnerPostId . '-1_3.jpg';

        if ($key == 1){
            if(isset($_FILES['partnerimage1']) && !empty($_FILES['partnerimage1']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage1']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->error_msg = 'Ошибка при загрузке файла 1';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }

        if ($key == 2){
            if(isset($_FILES['partnerimage2']) && !empty($_FILES['partnerimage2']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage2']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->error_msg = 'Ошибка при загрузке файла 2';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }

        if ($key == 3){
            if(isset($_FILES['partnerimage3']) && !empty($_FILES['partnerimage3']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage3']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->error_msg = 'Ошибка при загрузке файла 3';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }
        return $result;
    }
}