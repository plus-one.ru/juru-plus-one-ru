<?php
chdir('../');

require_once('Widget.admin.php');

$widget = new Widget();

$galleryId = $_POST['galleryId'];

if ($galleryId!="") {
    $query = sql_placeholder('SELECT * FROM images WHERE gallery_id=? ORDER BY order_num DESC', $galleryId);
    $widget->db->query($query);
    $images = $widget->db->results();

    $item = new stdClass();
    $item->photos = $images;
    $item->galleryId = $galleryId;

    $widget->smarty->assign('Item', $item);

    $result = $widget->smarty->fetch('include/gallery_images_loaded.tpl');

    header("Content-type: text/html; charset=UTF-8");
    header("Cache-Control: must-revalidate");
    header("Pragma: no-cache");
    header("Expires: -1");
    print json_encode($result);
}
