<?php
session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$postUrl = $_GET['post_url'];

$result = $blogClass->fetch_item($postUrl);

header("Content-type: application/json; charset=UTF-8");
header('Access-Control-Allow-Origin: *');
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($result);
?>