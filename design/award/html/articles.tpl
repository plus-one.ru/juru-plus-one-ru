<div id="ves-breadcrumbs">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li class="home">
                    <a href="{$fullRootUrl}" title="На главную страницу">Главная</a>
                </li>
                <li class="cms_page">
                    <strong>Блог</strong>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row visible-xs">
        <div class="container">

        </div>
    </div>
    <div class="content-wapper">
        <div class="row col2-right-layout">
            <section class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-column">
                <div id="content">
                    <div class="blog-list blogs-list">
                        <div class="blog-wrapper">
                            <div class="posts-list">
                                {if $articles}
                                    {foreach name=articles from=$articles item=a}
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 _itemfirst blog-col ">
                                            <div class="wrap-item">
                                                <div class="post-block">
                                                    <div class="blog-meta-block">
                                                        <h1 class="blog-title">
                                                            <a title="{$a->header|escape}" href="{$fullRootUrl}/post/{$a->url}">
                                                                {$a->header|escape}
                                                            </a>
                                                        </h1>
                                                    </div>
                                                    <div class="blog-image">
                                                        <a title="{$a->header|escape}" href="{$fullRootUrl}/post/{$a->url}">
                                                            <img alt="{$a->header|escape}" src="/files/blog/{$a->large_image}" class="img-responsive">
                                                        </a>
                                                        <span class="created-date">
                                                            <span class="day">{$a->date_day}</span>
                                                            <span class="month">{$a->date_month}</span>
                                                            <span class="year">{$a->date_year}</span>
                                                        </span>
                                                    </div>
                                                    <div class="blog-meta-block">
                                                        <div class="blog-desc">
                                                            {$a->annotation}
                                                        </div>

                                                        <div class="blog-tags">
                                                            <span>
                                                                <i class="fa fa-eye" style="margin-right: 3px;"></i>
                                                                Просмотров :
                                                            </span>
                                                            <a href="#" rel="nofollow" onclick="return false:">
                                                                {$a->viewed|escape}
                                                            </a>

                                                            <div style="margin: 4px 0;"><!-- /--></div>

                                                            <span>
                                                                <i class="fa fa-tags" style="margin-right: 3px;"></i>
                                                                Метки статьи :
                                                            </span>
                                                            {foreach item=tag from=$a->tags name=tag}
                                                                <a title="{$tag|escape}" href="{$fullRootUrl}/blog/tag/{$tag|escape}">
                                                                    <span>{$tag|escape}</span>
                                                                </a>
                                                                {if !$smarty.foreach.tag.last},{/if}
                                                            {/foreach}
                                                        </div>

                                                        <div class="blog-bottom vesclear">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div class="blog-updated">
                                                                    <span>Обновлено:</span>
                                                                    <strong>
                                                                        {$a->m_date_day} {$a->m_date_month} {$a->m_date_year} {$a->m_date_time}
                                                                    </strong>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <div style="text-align: right;" class="blog-hits">
                                                                    <span class="blog-readmore">
                                                                        <a title="" href="{$fullRootUrl}/post/{$a->url}">
                                                                            Читать...
                                                                        </a>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {**}
                                                        {*<span class="blog-readmore">*}
                                                            {*<a title="" href="{$fullRootUrl}/blog/{$a->url}">*}
                                                                {*Читать...*}
                                                            {*</a>*}
                                                        {*</span>*}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                {else}
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 _itemfirst blog-col ">
                                        <div class="wrap-item">
                                            <div class="post-block">
                                                <div class="blog-meta-block">
                                                    <h3 class="blog-title">
                                                        <p>Записей в блоге не найдено</p>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                {/if}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12 offcanvas-sidebar" id="ves-columns-right">
                {include file="include/blog_right_menu.tpl"}
            </aside>
        </div>
    </div>
</div>






{*<ul class="crumbs">*}
    {*<li><a href="{$fullRootUrl}">Главная</a><i class="divider">/</i></li>*}
    {*<li>Статьи</li>*}
{*</ul>*}
{*<div class="border-wrap">*}
    {*<h2 class="title"><span>Статьи</span></h2>*}
    {*{foreach name=articles from=$articles item=a}*}
    {*<div class="news_list">*}
        {*<a href="{$fullRootUrl}/articles/{$a->url}">*}
            {*<span>{$a->header|escape}</span>*}
        {*</a>*}
        {*{if $a->small_image}*}
        {*<a href="{$fullRootUrl}/articles/{$a->url}">*}
            {*<img src="/files/articles/{$a->small_image}" alt="{$a->header|escape}" title="{$a->header|escape}" />*}
        {*</a>*}
        {*{else}*}
        {*<a href="{$fullRootUrl}/articles/{$a->url}">*}
            {*<img src="/files/products/no_photo.png" />*}
        {*</a>*}
        {*{/if}*}
        {*{$a->annotation}*}
        {*<div style="clear: both; border-bottom: 1px solid #D0D0D0; height: 20px;"/><!-- /--></div>*}
    {*</div>*}
    {*{/foreach}*}
{*</div>*}