
<div class="popup-holder">
    <div id="popup1" class="popup">
        <div class="popup_block">
            <div class="section_title text-uppercase section_title--with-border">Анкета <br>номинанта</div>
            {}
        </div>
        <form action="#" class="popup_block">
            <div class="section_title text-uppercase">Оценка проекта</div>
            <p>Результативность/эффективность</p>
            <div class="custom-label-block">
                <label for="radio-1">
                    <input id="radio-1" name="group1" type="radio">
                    <span class="fake-label">1</span>
                </label>
                <label for="radio-2">
                    <input id="radio-2" name="group1" type="radio">
                    <span class="fake-label">2</span>
                </label>
                <label for="radio-3">
                    <input id="radio-3" name="group1" type="radio">
                    <span class="fake-label">3</span>
                </label>
                <label for="radio-4">
                    <input id="radio-4" name="group1" type="radio" checked>
                    <span class="fake-label">4</span>
                </label>
                <label for="radio-5">
                    <input id="radio-5" name="group1" type="radio">
                    <span class="fake-label">5</span>
                </label>
            </div>
            <p>Интегрированность в бизнес стратегию компании/существенность направлений ксо деятельности с точки зрения отраслевой деятельности</p>
            <div class="custom-label-block">
                <label for="radio-6">
                    <input id="radio-6" name="group2" type="radio">
                    <span class="fake-label">1</span>
                </label>
                <label for="radio-7">
                    <input id="radio-7" name="group2" type="radio">
                    <span class="fake-label">2</span>
                </label>
                <label for="radio-8">
                    <input id="radio-8" name="group2" type="radio">
                    <span class="fake-label">3</span>
                </label>
                <label for="radio-9">
                    <input id="radio-9" name="group2" type="radio">
                    <span class="fake-label">4</span>
                </label>
                <label for="radio-10">
                    <input id="radio-10" name="group2" type="radio">
                    <span class="fake-label">5</span>
                </label>
            </div>
            <p>Инновационность</p>
            <div class="custom-label-block">
                <label for="radio-11">
                    <input id="radio-11" name="group3" type="radio">
                    <span class="fake-label">1</span>
                </label>
                <label for="radio-12">
                    <input id="radio-12" name="group3" type="radio">
                    <span class="fake-label">2</span>
                </label>
                <label for="radio-13">
                    <input id="radio-13" name="group3" type="radio">
                    <span class="fake-label">3</span>
                </label>
                <label for="radio-14">
                    <input id="radio-14" name="group3" type="radio">
                    <span class="fake-label">4</span>
                </label>
                <label for="radio-15">
                    <input id="radio-15" name="group3" type="radio">
                    <span class="fake-label">5</span>
                </label>
            </div>
            <p>Общая оценка проекта</p>
            <div class="custom-label-block">
                <label for="radio-16">
                    <input id="radio-16" name="group4" type="radio">
                    <span class="fake-label">1</span>
                </label>
                <label for="radio-17">
                    <input id="radio-17" name="group4" type="radio">
                    <span class="fake-label">2</span>
                </label>
                <label for="radio-18">
                    <input id="radio-18" name="group4" type="radio">
                    <span class="fake-label">3</span>
                </label>
                <label for="radio-19">
                    <input id="radio-19" name="group4" type="radio">
                    <span class="fake-label">4</span>
                </label>
                <label for="radio-20">
                    <input id="radio-20" name="group4" type="radio">
                    <span class="fake-label">5</span>
                </label>
            </div>
            <p>======================================</p>
            <button class="btn btn--form">CОХРАНИТЬ</button>
        </form>
    </div>
</div>
