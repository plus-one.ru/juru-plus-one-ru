<section class="section">
    <div class="container">
        <h1 class="section_title text-center text-uppercase">ВХОД В КАБИНЕТ</h1>
        <form action="/login" method="post" class="login-form form-validation">
            <div class="form-input">
                <label for="login-form_field-1">Логин</label>
                <input name="log-in" type="email" id="login-form_field-1" data-required="true" data-interactive="false" />
                {if $error_login}
                    <div class="error-text" style="display: inline-block">{$error_login}</div>
                {else}
                    <div class="error-text">Неверный E-mail</div>
                {/if}
            </div>
            <div class="form-input">
                <label for="login-form_field-2">Пароль</label>
                <input name="pas-d" type="password" id="login-form_field-2" data-required="true" data-interactive="false" />
                <div class="error-text">Неверный пароль</div>
            </div>
            <div class="login-form_btn-box">
                <button type="submit" class="btn">ВОЙТИ</button>
            </div>
        </form>
    </div>
</section>