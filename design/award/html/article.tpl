<div id="ves-breadcrumbs">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li class="home">
                    <a href="{$fullRootUrl}" title="На главную страницу">Главная</a>
                </li>
                <li class="home">
                    <a href="{$fullRootUrl}/blog/" title="Блог">Блог</a>
                </li>
                <li class="cms_page">
                    <strong>{$article->header|escape}</strong>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row visible-xs">
        <div class="container">

        </div>
    </div>
    <div class="content-wapper">
        <div class="row col2-right-layout">
            <section class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-column">
                <div id="content">
                    <div class="blog-post">
                        <div class="blog-wrapper">
                            <div class="blog-content">
                                <div class="blog-container">
                                    <div class="blog-meta">
                                        <h1 class="blog-title">{$article->header|escape}</h1>
                                        <span class="created-date">
                                            <i class="fa fa-calendar"></i>
                                            <span class="day">{$article->date_day}</span>
                                            <span class="month">{$article->date_month}</span>
                                            <span class="year">{$article->date_year}</span>
                                        </span>
                                    </div>
                                    <div class="blog-image">
                                        <img title="{$article->header|escape}" src="/files/blog/{$article->large_image}">
                                    </div>
                                    <div class="blog-detail">
                                        {$article->body}
                                    </div>
                                </div>

                                <div class="blog-tags">
                                    <span>
                                        <i class="fa fa-eye" style="margin-right: 3px;"></i>
                                        Просмотров :
                                    </span>
                                    <a href="#" rel="nofollow" onclick="return false:">
                                        {$article->viewed|escape}
                                    </a>

                                    <div style="margin: 4px 0;"><!-- /--></div>

                                    <span>
                                        <i class="fa fa-tags" style="margin-right: 3px;"></i>
                                        Метки статьи :
                                    </span>
                                    {foreach item=tag from=$article->tags name=tag}
                                        <a title="{$tag|escape}" href="{$fullRootUrl}/blog/tag/{$tag|escape}">
                                            <span>{$tag|escape}</span>
                                        </a>
                                        {if !$smarty.foreach.tag.last},{/if}
                                    {/foreach}
                                </div>

                                <div class="blog-bottom vesclear">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="blog-updated">
                                            <span>Обновлено:</span>
                                            <strong>
                                                {$article->m_date_day} {$article->m_date_month} {$article->m_date_year} {$article->m_date_time}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="blog-hits" style="text-align: right;">
                                            <span class="blog-readmore">
                                                <a title="Все статьи" href="{$fullRootUrl}/blog/">
                                                    Все статьи
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                {*<div class="blog-social">*}
                                {*<div class="social-sharing vesclear">*}
                                {*<div class="social-label">Share This:</div>*}

                                {*</div>*}
                                {*</div>*}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12 offcanvas-sidebar" id="ves-columns-right">
                {include file="include/blog_right_menu.tpl"}
            </aside>
        </div>
    </div>
</div>