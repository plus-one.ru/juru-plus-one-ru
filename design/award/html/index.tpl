<!DOCTYPE html>
<html dir="ltr" class="ltr" lang="ru">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="robots" content="all" />
    <title>{$title|escape}</title>
    <meta name="description" content="{$description|escape}" />
    <meta name="keywords" content="{$keywords|escape}" />
    <meta property="og:title" content="{$ogTitle|escape}" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{$description|escape}" />
    <meta property="og:image" content="http://{$root_url}/{$ogImage}" />
    <meta property="og:site_name" content="{$ogTitle|escape}" />
    <meta property="og:url" content="http://{$root_url}/{$ogUrl}" />
    <meta name="twitter:title" content="{$ogTitle|escape}" />
    <meta name="twitter:description" content="{$description|escape}" />
    <meta name="twitter:url" content="http://{$root_url}/{$ogUrl}" />
    <meta name="twitter:image" content="http://{$root_url}/{$ogImage}" />
    <meta name="twitter:image:alt" content="{$ogTitle|escape}" />
    <meta name="twitter:site" content="{$ogTitle|escape}" />
    <link rel="image_src" href="http://{$root_url}/{$ogImage}">

    <link rel="apple-touch-icon" sizes="57x57" href="/design/{$settings->theme|escape}/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/design/{$settings->theme|escape}/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/design/{$settings->theme|escape}/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/design/{$settings->theme|escape}/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/design/{$settings->theme|escape}/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/design/{$settings->theme|escape}/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/design/{$settings->theme|escape}/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/design/{$settings->theme|escape}/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/design/{$settings->theme|escape}/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/design/{$settings->theme|escape}/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/design/{$settings->theme|escape}/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/design/{$settings->theme|escape}/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/design/{$settings->theme|escape}/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/design/{$settings->theme|escape}/images/favicon/manifest.json">
    <link rel="shortcut icon" href="/design/{$settings->theme|escape}/images/favicon/favicon.ico" type="image/x-icon"/>

    <base href="https://{$root_url}/">
    <link media="all" rel="stylesheet" href="https://{$root_url}/design/{$settings->theme|escape}/css/main.css?v=2">
</head>
<body>
<div id="wrapper" class="login-page">
    <header id="header">
        <div class="container">
            <div class="logo">
                <a href="./">
                    <img src="https://{$root_url}/design/{$settings->theme|escape}/images/logo.svg" alt="Plus 1" width="130" height="68" />
                </a>
            </div>
        </div>
    </header>
    <main id="main" role="main">
        {$content}
    </main>
</div>
<div class="page-bg">
    <img src="https://{$root_url}/design/{$settings->theme|escape}/images/page-bg.svg" alt="image description" />
</div>
<script src="https://{$root_url}/design/{$settings->theme|escape}/js/jquery-3.2.1.min.js" defer></script>
<script src="https://{$root_url}/design/{$settings->theme|escape}/js/jquery.main.js?v=2" defer></script>
</body>
</html>