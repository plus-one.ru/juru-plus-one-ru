<section class="section">
    <div class="container nominee-area">
        <h1 class="section_title text-uppercase section_title--with-border">
            {$nomination->name|escape}
        </h1>
        <div class="nominee-block">
            <div class="nominee-block_box">
                <span>Название проекта</span>
                <span>Организация</span>
                <span class="text-right">Балл</span>
            </div>
            {foreach name=nominee from=$nominees item=nominee}
                <a href="#popup{$nominee->id}" class="nominee-block_box lightbox">
                    <span>{$nominee->name|escape}</span>
                    <span>{$nominee->header|escape}</span>
                    <span class="text-right">
                        {$nominee->averageScore|escape}
                    </span>
                </a>
            {/foreach}
        </div>

        <a href="/freeze/{$nomination->url}" class="btn nominee__btn{if $checkFreeze == 0}--active-state{/if}">ЗАВЕРШИТЬ</a>

        <div class="text-description text-center">
            После нажатия на кнопку, изменение оценки станет недоступно
        </div>
    </div>
</section>


{foreach name=nominee from=$nominees item=nominee}
    <div class="popup-holder">
        <div id="popup{$nominee->id}" class="popup">
            <div class="popup_block">
                <div class="section_title text-uppercase section_title--with-border">Анкета <br>номинанта</div>
                {$nominee->body}
            </div>
            <form action="" class="popup_block popup__form form-validation" method="post">
                <input type="hidden" name="nomineeid" value="{$nominee->id}">
                <input type="hidden" name="nominationid" value="{$nomination->id}">
                <div class="section_title text-uppercase">Оценка проекта</div>
                <p>Результативность/эффективность</p>
                <div class="custom-label-block form-input">
                    <label for="radio-1_{$nominee->id}">
                        <input id="radio-1_{$nominee->id}" name="group1_{$nominee->id}" value="1" type="radio" data-required="true" {if $nominee->group_1 == 1}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">1</span>
                    </label>
                    <label for="radio-2_{$nominee->id}">
                        <input id="radio-2_{$nominee->id}" name="group1_{$nominee->id}" value="2" type="radio" data-required="true" {if $nominee->group_1 == 2}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">2</span>
                    </label>
                    <label for="radio-3_{$nominee->id}">
                        <input id="radio-3_{$nominee->id}" name="group1_{$nominee->id}" value="3" type="radio" data-required="true" {if $nominee->group_1 == 3}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">3</span>
                    </label>
                    <label for="radio-4_{$nominee->id}">
                        <input id="radio-4_{$nominee->id}" name="group1_{$nominee->id}" value="4" type="radio" data-required="true" {if $nominee->group_1 == 4}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">4</span>
                    </label>
                    <label for="radio-5_{$nominee->id}">
                        <input id="radio-5_{$nominee->id}" name="group1_{$nominee->id}" value="5" type="radio" data-required="true" {if $nominee->group_1 == 5}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">5</span>
                    </label>
                </div>
                <p>Интегрированность в стратегию организации</p>
                <div class="custom-label-block form-input">
                    <label for="radio-6_{$nominee->id}">
                        <input id="radio-6_{$nominee->id}" name="group2_{$nominee->id}" value="1" type="radio" data-required="true" {if $nominee->group_2 == 1}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">1</span>
                    </label>
                    <label for="radio-7_{$nominee->id}">
                        <input id="radio-7_{$nominee->id}" name="group2_{$nominee->id}" value="2" type="radio" data-required="true" {if $nominee->group_2 == 2}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">2</span>
                    </label>
                    <label for="radio-8_{$nominee->id}">
                        <input id="radio-8_{$nominee->id}" name="group2_{$nominee->id}" value="3" type="radio" data-required="true" {if $nominee->group_2 == 3}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">3</span>
                    </label>
                    <label for="radio-9_{$nominee->id}">
                        <input id="radio-9_{$nominee->id}" name="group2_{$nominee->id}" value="4" type="radio" data-required="true" {if $nominee->group_2 == 4}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">4</span>
                    </label>
                    <label for="radio-10_{$nominee->id}">
                        <input id="radio-10_{$nominee->id}" name="group2_{$nominee->id}" value="5" type="radio" data-required="true" {if $nominee->group_2 == 5}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">5</span>
                    </label>
                </div>
                <p>Новизна</p>
                <div class="custom-label-block form-input">
                    <label for="radio-11_{$nominee->id}">
                        <input id="radio-11_{$nominee->id}" name="group3_{$nominee->id}" value="1" type="radio" data-required="true" {if $nominee->group_3 == 1}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">1</span>
                    </label>
                    <label for="radio-12_{$nominee->id}">
                        <input id="radio-12_{$nominee->id}" name="group3_{$nominee->id}" value="2" type="radio" data-required="true" {if $nominee->group_3 == 2}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">2</span>
                    </label>
                    <label for="radio-13_{$nominee->id}">
                        <input id="radio-13_{$nominee->id}" name="group3_{$nominee->id}" value="3" type="radio" data-required="true" {if $nominee->group_3 == 3}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">3</span>
                    </label>
                    <label for="radio-14_{$nominee->id}">
                        <input id="radio-14_{$nominee->id}" name="group3_{$nominee->id}" value="4" type="radio" data-required="true" {if $nominee->group_3 == 4}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">4</span>
                    </label>
                    <label for="radio-15_{$nominee->id}">
                        <input id="radio-15_{$nominee->id}" name="group3_{$nominee->id}" value="5" type="radio" data-required="true" {if $nominee->group_3 == 5}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">5</span>
                    </label>
                </div>
                <p>Острота решаемой проблемы</p>
                <div class="custom-label-block form-input">
                    <label for="radio-16_{$nominee->id}">
                        <input id="radio-16_{$nominee->id}" name="group4_{$nominee->id}" value="1" type="radio" data-required="true" {if $nominee->group_4 == 1}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">1</span>
                    </label>
                    <label for="radio-17_{$nominee->id}">
                        <input id="radio-17_{$nominee->id}" name="group4_{$nominee->id}" value="2" type="radio" data-required="true" {if $nominee->group_4 == 2}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">2</span>
                    </label>
                    <label for="radio-18_{$nominee->id}">
                        <input id="radio-18_{$nominee->id}" name="group4_{$nominee->id}" value="3" type="radio" data-required="true" {if $nominee->group_4 == 3}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">3</span>
                    </label>
                    <label for="radio-19_{$nominee->id}">
                        <input id="radio-19_{$nominee->id}" name="group4_{$nominee->id}" value="4" type="radio" data-required="true" {if $nominee->group_4 == 4}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">4</span>
                    </label>
                    <label for="radio-20_{$nominee->id}">
                        <input id="radio-20_{$nominee->id}" name="group4_{$nominee->id}" value="5" type="radio" data-required="true" {if $nominee->group_4 == 5}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">5</span>
                    </label>
                </div>


                <p>Оценка эксперта</p>
                <div class="custom-label-block form-input">
                    <label for="radio-21_{$nominee->id}">
                        <input id="radio-21_{$nominee->id}" name="group5_{$nominee->id}" value="1" type="radio" data-required="true" {if $nominee->group_5 == 1}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">1</span>
                    </label>
                    <label for="radio-22_{$nominee->id}">
                        <input id="radio-22_{$nominee->id}" name="group5_{$nominee->id}" value="2" type="radio" data-required="true" {if $nominee->group_5 == 2}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">2</span>
                    </label>
                    <label for="radio-23_{$nominee->id}">
                        <input id="radio-23_{$nominee->id}" name="group5_{$nominee->id}" value="3" type="radio" data-required="true" {if $nominee->group_5 == 3}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">3</span>
                    </label>
                    <label for="radio-24_{$nominee->id}">
                        <input id="radio-24_{$nominee->id}" name="group5_{$nominee->id}" value="4" type="radio" data-required="true" {if $nominee->group_5 == 4}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">4</span>
                    </label>
                    <label for="radio-25_{$nominee->id}">
                        <input id="radio-25_{$nominee->id}" name="group5_{$nominee->id}" value="5" type="radio" data-required="true" {if $nominee->group_5 == 5}checked{/if} {if $nominee->frozen == 1}disabled{/if}>
                        <span class="fake-label">5</span>
                    </label>
                </div>

                <p>Комментарий эксперта</p>
                <div class="custom-label-block form-input">
                    <textarea name="comment_{$nominee->id}">{$nominee->comment}</textarea>
                </div>

                {if $nominee->frozen == 0}
                <button class="btn btn--form">CОХРАНИТЬ</button>
                {/if}
                <div class="popup__form__error-text">
                    Перед сохранением необходимо установить оценки по всем параметрам
                </div>
            </form>
        </div>
    </div>
{/foreach}
