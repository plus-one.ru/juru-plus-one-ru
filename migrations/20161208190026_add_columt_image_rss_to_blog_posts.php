<?php

use Phinx\Migration\AbstractMigration;

class AddColumtImageRssToBlogPosts extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('image_rss', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('image_rss')
            ->save();
    }
}
