<?php

use Phinx\Migration\AbstractMigration;

class AddTableModules extends AbstractMigration
{
    private $tablename = 'modules';

    public function up()
    {
        $this->table($this->tablename, array('id' => 'module_id'))
            ->addColumn('class', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('name', 'string', ['limit'=>255, 'null' => false, 'default'=>0])
            ->addColumn('valuable', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->addColumn('url', 'string', ['limit'=>255, 'null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
