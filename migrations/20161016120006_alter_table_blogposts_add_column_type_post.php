<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsAddColumnTypePost extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('type_post', 'integer', ['limit' => 11, 'null' => false, 'default' => 1])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('type_post')
            ->save();
    }
}
