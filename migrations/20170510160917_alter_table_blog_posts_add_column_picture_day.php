<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnPictureDay extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('picture_day', 'integer', ['null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)->removeColumn('picture_day')->save();
    }
}
