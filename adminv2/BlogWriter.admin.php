<?PHP
require_once('Widget.admin.php');
require_once('BlogTags.admin.php');
require_once('PostTags.admin.php');
require_once('../placeholder.php');

/**
 * Class BlogWriter
 */
class BlogWriter extends Widget
{
    var $item;
    var $uploaddir = '../files/writers/'; # Папка для хранения картинок (default)
    var $large_image_width = "600";
    var $large_image_height = "600";
    private $tableName = 'blogwriters';
  
    function BlogWriter(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $item_id = intval($this->param('item_id'));

        if(isset($_POST['name'])){
            $this->check_token();

            $this->item->name = $_POST['name'];

            if(isset($_POST['enabled']) && $_POST['enabled']==1) {
                $this->item->enabled = 1;
            }
            else{
                $this->item->enabled = 0;
            }
            $this->item->leader = $_POST['leader'];
            $this->item->body = $_POST['body'];

            ## Не допустить одинаковые URL статей
    	    $query = sql_placeholder('SELECT COUNT(*) AS count FROM ' . $this->tableName . ' WHERE name=? AND id!=?', $this->item->name, $item_id);
            $this->db->query($query);
            $res = $this->db->result();

  		    if(empty($this->item->name)){
			    $this->error_msg = $this->lang->ENTER_TITLE;
  		    }
  		    elseif($res->count>0){
			    $this->error_msg = 'Писатель с таким названием уже существует. Укажите другое.';
  		    }
            else{
  			    if(empty($item_id)) {
                    $this->item->image = null;
                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }
  	    		else{
                    if (is_null($this->update_article($item_id))){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }

                $this->add_fotos($item_id);

                $get = $this->form_get(array('section'=>'BlogWriters'));

                if (empty($this->error_msg)){
                    if(isset($_GET['from'])){
                        header("Location: ".$_GET['from']);
                    }
                    else{
                        header("Location: index.php$get");
                    }
                }
  		    }
  	    }
  	    elseif (!empty($item_id)){
		    $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
  	    }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новый автор';
		}
		else{
			$this->title = 'Изменение автора: ' . $this->item->name;
		}


        $blogTagsClass = new BlogTags();

        $blogTags = $blogTagsClass->getTags();

        foreach ($blogTags AS $k=>$blogTag){
            $query = sql_placeholder("SELECT id, name_lead FROM post_tags WHERE parent=? ORDER BY name ASC", $blogTag->id);
            $this->db->query($query);
            $blogTags[$k]->items = $this->db->results();
        }

//        echo "<pre>";
//        print_r($blogTags);
//        echo "</pre>";

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
        $this->smarty->assign('blogTags', $blogTags);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);



		$this->body = $this->smarty->fetch('blogwriter.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
        echo $query;
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);
//echo $query;
        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $largeuploadfile = $itemId.".jpg";

        /// Загрузка большой картинки
        $large_image_uploaded = false;

        if(isset($_FILES['large_image']) && !empty($_FILES['large_image']['tmp_name'])){
            if (!move_uploaded_file($_FILES['large_image']['tmp_name'], $this->uploaddir.$largeuploadfile)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $large_image_uploaded = true;
            }
        }

        if($large_image_uploaded){
            $upload_folder = $this->uploaddir.$largeuploadfile;
            $this->im_image_resize($upload_folder, $upload_folder, $this->large_image_width, $this->large_image_height);
            @chmod($this->uploaddir.$largeuploadfile, 0644);
            $this->db->query("UPDATE {$this->tableName} SET image='$largeuploadfile' WHERE id={$itemId}");
            $result = true;
        }

        return $result;
    }
}
