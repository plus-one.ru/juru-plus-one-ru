<?PHP
require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class Article
 */
class Judge extends Widget
{
    var $item;
    private $salt = 'simpla';
    private $tableName = 'users';
  
    function Judge(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $item_id = intval($this->param('item_id'));

        if(isset($_POST['name']) && isset($_POST['login'])){
            $this->check_token();

            $this->item->name = $_POST['name'];
            $this->item->login = $_POST['login'];

            if (!empty($_POST['password'])){
                $this->item->password = md5($_POST['password'] . $this->salt);
            }



            if(isset($_POST['enabled']) && $_POST['enabled']==1) {
                $this->item->enabled = 1;
            }
            else{
                $this->item->enabled = 0;
            }

            ## Не допустить одинаковые URL статей
    	    $query = sql_placeholder('SELECT COUNT(*) AS count FROM ' . $this->tableName . ' WHERE login=? AND id!=?', $this->item->login, $item_id);
            $this->db->query($query);
            $res = $this->db->result();

  		    if(empty($this->item->name) || empty($this->item->login) || empty($this->item->password)){
			    $this->error_msg = "Заполните все поля";
  		    }
  		    elseif($res->count>0){
			    $this->error_msg = 'Судья с таким логином уже зарегистрирован.';
  		    }
            else{
  			    if(empty($item_id)) {
                    $this->item->created = date('Y-m-d H:i:s', time());
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }
  	    		else{
                    $this->item->modified = date('Y-m-d H:i:s', time());
                    if (is_null($this->update_article($item_id))){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }

                $get = $this->form_get(array('section'=>'Judges'));
			
			    if(isset($_GET['from'])){
				    header("Location: ".$_GET['from']);
			    }
			    else{
				    header("Location: index.php$get");
			    }
  		    }
  	    }
  	    elseif (!empty($item_id)){
		    $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
  	    }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новый судья';
		}
		else{
			$this->title = 'Изменение судьи: ' . $this->item->name;
		}

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);

		$this->body = $this->smarty->fetch('judge.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
//        echo $query;die();
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);

        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }
}