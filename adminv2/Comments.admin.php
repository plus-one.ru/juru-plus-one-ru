<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class Comments
 */
class Comments extends Widget
{
    private $tableName = 'comments';

    /**
     * @param $parent
     */
    function Comments(&$parent)
    {
        parent::Widget($parent);
        $this->prepare();
    }

    /**
     *
     */
    function prepare()
    {
        if (isset($_GET['act']) && $_GET['act'] == 'delete' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);
            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_enabled'])) {
            $this->check_token();

            $this->setEnable($_GET['set_enabled'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_show'])) {
            $this->check_token();

            $this->setShow($_GET['set_show'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }
    }

    /**
     *
     */
    function fetch()
    {
        $this->title = 'Комментарии';

        $items = $this->getItems();

        foreach ($items as $key => $item) {
            $items[$key]->edit_get = $this->form_get(array('section' => 'Comment', 'item_id' => $item->id, 'token' => $this->token));
        }

        $this->smarty->assign('Items', $items);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Lang', $this->lang);
        $this->body = $this->smarty->fetch('comments.tpl');
    }


    function getItems(){
        $query = sql_placeholder("SELECT blogposts.id, blogposts.name
                                   FROM " . $this->tableName . "
                                    INNER JOIN blogposts on blogposts.id = comments.nominee_id
                                    GROUP BY blogposts.id
                                    ORDER BY name ASC");
        $this->db->query($query);
        return $this->db->results();
    }

}
