<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class BlogTags
 */
class Judges extends Widget
{
    private $tableName = 'users';

    /**
     * @param $parent
     */
    function Judges(&$parent)
    {
        parent::Widget($parent);
        $this->prepare();
    }

    /**
     *
     */
    function prepare()
    {
        if (isset($_GET['act']) && $_GET['act'] == 'delete' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);
            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_enabled'])) {
            $this->check_token();

            $this->setEnable($_GET['set_enabled'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_show'])) {
            $this->check_token();

            $this->setShow($_GET['set_show'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }
    }

    /**
     *
     */
    function fetch()
    {
        $this->title = 'Судьи';

        $items = $this->getItems();

        foreach ($items as $key => $item) {
            $items[$key]->edit_get = $this->form_get(array('section' => 'Judge', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->delete_get = $this->form_get(array('act' => 'delete', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->enable_get = $this->form_get(array('set_enabled'=>$item->id, 'item_id'=>$this->id, 'token'=>$this->token));
            $items[$key]->show_get = $this->form_get(array('set_show'=>$item->id, 'item_id'=>$this->id, 'token'=>$this->token));
        }

        $this->smarty->assign('Items', $items);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Lang', $this->lang);
        $this->body = $this->smarty->fetch('judges.tpl');
    }


    function getItems(){
        $query = sql_placeholder("SELECT *,
                                    DATE_FORMAT(created, '%d.%m.%Y') AS date_created,
                                    DATE_FORMAT(created, '%H:%i') AS time_created,
                                    DATE_FORMAT(modified, '%d.%m.%Y') AS date_modified,
                                    DATE_FORMAT(modified, '%H:%i') AS time_modified
                                  FROM " . $this->tableName . " ORDER BY name ASC");
        $this->db->query($query);
        $items = $this->db->results();


        $query = sql_placeholder("SELECT id, name, url FROM blogtags WHERE enabled = 1 AND isshow = 1");
        $this->db->query($query);
        $nominations = $this->db->results();

        $res = [];
        foreach ($nominations AS $nomination){
            $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM blogposts WHERE tags = ? AND enabled = 1", $nomination->id);
            $this->db->query($query);
            $nomenee = $this->db->result();
            $res[$nomination->url] = [
                'nominationId' => $nomination->id,
                'nominationName' => $nomination->name,
                'count' => $nomenee->cnt,
            ];

        }

        foreach ($items AS $k=>$v) {

            foreach ($res as $resKey => $r){
                $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM assessments WHERE nomination_id = ? AND judge_id = ?", $r['nominationId'], $v->id);
                $this->db->query($query);
                $judge = $this->db->result();

                $items[$k]->nominations->$resKey->info = (object) $r;
                $items[$k]->nominations->$resKey->jcount = $judge->cnt;
                $items[$k]->nominations->$resKey->tcount = $r['count'];

                $items[$k]->nominations->$resKey->class = "text-danger";
                if ($judge->cnt == $r['count'] && $r['count'] != 0){
                    $items[$k]->nominations->$resKey->class = "text-success";
                }
            }
        }


        // считаем в каких номинациях судья все оценки поставил
        // сначала считаем сколько всего номинантов в каждой номинации

        // 1 - экономика
        // 2 - экология
        // 3 - общество
//        $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM blogposts WHERE tags = 1 AND enabled = 1");
//        $this->db->query($query);
//        $economyNomenee = $this->db->result();
//        $economyNomeneeTotalCount = $economyNomenee->cnt;
//
//        $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM blogposts WHERE tags = 2 AND enabled = 1");
//        $this->db->query($query);
//        $ecologyNomenee = $this->db->result();
//        $ecologyNomeneeTotalCount = $ecologyNomenee->cnt;
//
//        $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM blogposts WHERE tags = 3 AND enabled = 1");
//        $this->db->query($query);
//        $communityNomenee = $this->db->result();
//        $communityNomeneeTotalCount = $communityNomenee->cnt;
//
//        foreach ($items AS $k=>$v){
//            // считаем за скольких проголосовал этот судья
//            $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM assessments WHERE nomination_id = 1 AND judge_id = ?", $v->id);
//            $this->db->query($query);
//            $economyJudge = $this->db->result();
//            $items[$k]->economyJudgeTotalCount = $economyJudge->cnt;
//            $items[$k]->economyNomeneeTotalCount = $economyNomeneeTotalCount;
//
//            $items[$k]->economyClass = "text-danger";
//            if ($economyJudge->cnt == $economyNomeneeTotalCount){
//                $items[$k]->economyClass = "text-success";
//            }
//
//            $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM assessments WHERE nomination_id = 2 AND judge_id = ?", $v->id);
//            $this->db->query($query);
//            $ecologyJudge = $this->db->result();
//            $items[$k]->ecologyJudgeTotalCount = $ecologyJudge->cnt;
//            $items[$k]->ecologyNomeneeTotalCount = $ecologyNomeneeTotalCount;
//
//            $items[$k]->ecologyClass = "text-danger";
//            if ($ecologyJudge->cnt == $ecologyNomeneeTotalCount){
//                $items[$k]->ecologyClass = "text-success";
//            }
//
//            $query = sql_placeholder("SELECT COUNT(id) AS cnt FROM assessments WHERE nomination_id = 3 AND judge_id = ?", $v->id);
//            $this->db->query($query);
//            $communityJudge = $this->db->result();
//            $items[$k]->communityJudgeTotalCount = $communityJudge->cnt;
//            $items[$k]->communityNomeneeTotalCount = $communityNomeneeTotalCount;
//
//            $items[$k]->communityClass = "text-danger";
//            if ($communityJudge->cnt == $communityNomeneeTotalCount){
//                $items[$k]->communityClass = "text-success";
//            }
//        }


        return $items;
    }

}
