<?PHP
require_once('Widget.admin.php');

class Login extends Widget{
	var $salt = 'simpla'; // Соль для шифрования пароля

	function Login(&$parent){
		Widget::Widget($parent);
		$this->prepare();
	}

	function prepare()
	{
	    // Разлогинить если передан соответствующий параметр
		if (isset($_GET['action']) && $_GET['action']=='logout') {

			unset($_SESSION['adminarea']['admin']);
			unset($_SESSION['adminarea']['login']);
			unset($_SESSION['adminarea']['user_password']);
			unset($_SESSION['adminarea']['group']);

			header("Location: " . $this->root_admin_url);
			exit();
		}
		elseif (isset($_GET['action']) && $_GET['action']=='changepassword'){
			return $this->actionChangePassword();
		}
		else{
            return $this->action_login();
        }
	}


	function actionChangePassword(){

        if (!empty($_POST['oldpassword']) && !empty($_POST['newpassword']) && !empty($_POST['newpasswordre'])){

            // для начала проверим, правильно ли указан старый пароль
            $login = 'admin';
            $password = substr($_POST['oldpassword'], 0, 25);
            $encpassword = md5($password.$this->salt);

            // Выбираем из базы пользователя с таки логином и паролем
            $query = sql_placeholder('select * from users_admin where login=? and password=? limit 1', $login, $encpassword);
            $this->db->query($query);
            $user = $this->db->result();

            // если старый пароль указан верно - то приступаем к смене пароля
            if (!empty($user)){
                if ($_POST['newpassword'] === $_POST['newpasswordre']){

                    $newPassword = substr($_POST['newpassword'], 0, 25);
                    $encNewPassword = md5($newPassword.$this->salt);

                    $query = sql_placeholder('UPDATE users_admin SET password=? WHERE id=? ', $encNewPassword, $user->id);
                    $this->db->query($query);

                    // пароль сменили, разлогиниваем пользователя, и отправляем на главную
                    unset($_SESSION['adminarea']['admin']);
                    unset($_SESSION['adminarea']['login']);
                    unset($_SESSION['adminarea']['user_password']);
                    unset($_SESSION['adminarea']['group']);

                    header("Location: " . $this->root_admin_url);
                    exit();

                }
                else{
                    $this->smarty->assign('error', "Новые пароли различаются, повторите попытку");
                }
            }
            else{
                $this->smarty->assign('error', "Ошибка в поле \"старый пароль\"");
            }
        }
        else{
            $this->smarty->assign('error', "Все поля являются обязательными для заполнения");
        }

	}

	function action_login()
	{
		// Если запостили login и пароль, попытаемся залогинить
		if (!empty($_POST['login']) && !empty($_POST['password']))
		{
		    // Берем "голые" данные, но это безопасно
			$login = substr($_POST['login'], 0, 100);
			$password = substr($_POST['password'], 0, 25);
			$encpassword = md5($password.$this->salt);

			// Выбираем из базы пользователя с таки логином и паролем
			$query = sql_placeholder('select * from users_admin where login=? and password=? limit 1', $login, $encpassword);
			$this->db->query($query);
			$user = $this->db->result();

			if (empty($user))
			{
				// Если такого нет
				$error = 'Неверный login или пароль';
			}
			elseif ($user->enabled == 0)
			{	
				// Если пользователь не активен
				$error = 'Ваша учетная запись отключена. Пожалуйста, обратитесь к администратору';
			}
			if (!empty($error))
			{
			    // В случае ошибки предлагаем опять форму логина
				$this->smarty->assign('error_login', $error);
				$this->smarty->assign('login', $login);
				$this->smarty->assign('password', $password);
			}
			else
			{
			    // Если все хорошо - залогиниваем, и переходим на главную
                $_SESSION['adminarea']['admin'] = true;
                $_SESSION['adminarea']['login'] = $login;
				$_SESSION['adminarea']['user_password'] = $encpassword;
				$_SESSION['adminarea']['group'] = $user->group_id;
				
				header("Location: ". $this->root_admin_url);
				exit();
			}
		}
		else
		{
			if ($_POST) {
                $this->smarty->assign('error_login', 'Укажите логин и пароль');
            }
		}
	}
	
	function fetch()
	{

        if (isset($_GET['action']) && $_GET['action']=='changepassword'){
            $this->body = $this->smarty->fetch('change_password.tpl');
            $this->title = 'Смена пароля администратора';
            return $this->body;
        }
        else{
            $this->body = $this->smarty->fetch('login.tpl');
            $this->title = 'Вход на сайт';
            return $this->body;
        }
	}

}
