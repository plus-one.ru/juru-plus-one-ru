<?php

////////////////////////////////////////////////
// class Widget - базовый класс для всех модулей
////////////////////////////////////////////////

require_once('../Config.class.php');
require_once('../Database.class.php');
require_once('../placeholder.php');
require_once('Setka.admin.php');

class Widget
{
	var $params = array(); // get-параметры
    var $title = null; // meta-title
    var $description = null; // meta-description
    var $keywords = null; // meta-keywords
    var $body = null; // тело страницы
    var $error_msg = null; // сообщение об ошибке

    var $db; // база данный (класса Database)
	var $smarty; //  шаблонизатор
	var $config; // Config.class.php - класс с найтройками
    var $settings; // настройки сайта (из таблицы settings)
	var $lang;  // язык панели управления
    var $parent;  // родитель текущего объекта (в плане иерархии simpla)
    var $main_currency;  // родитель текущего объекта (в плане иерархии simpla)
    var $use_gd = true;
    var $root_url = '';
    var $root_dir = '';
    var $root_admin_url = '';
    var $user; // пользователь, если залогинен
    /** @var Setka */
    var $setka;
  
    function stripslashes_recursive($var)
    {
    	if(is_array($var))
    	  foreach($var as $k=>$v)
    	    $var[$k] = $this->stripslashes_recursive($v);
    	  else
    	    $var = stripcslashes($var);
    	return $var;
    }

    function Widget(&$parent)
    {
       if (is_object($parent))
        {
        	$this->parent=$parent;
            $this->db=&$parent->db;
            $this->smarty=&$parent->smarty;
            $this->config=&$parent->config;
            $this->params=&$parent->params;
            $this->settings=&$parent->settings;
            $this->main_currency=&$parent->main_currency;
            $this->lang=&$parent->lang;
            $this->root_url=&$parent->root_url;
            $this->root_dir=&$parent->root_dir;
            $this->token=&$parent->token;
            $this->user=&$parent->user;
            $this->root_admin_url = &$parent->root_admin_url;
            $this->setka=&$parent->setka;
        }
        else
        {
			if(get_magic_quotes_gpc())
			{
			  $_POST = $this->stripslashes_recursive($_POST);
			  $_GET = $this->stripslashes_recursive($_GET);
			}

			$this->config=new Config();

            require_once("../Smarty/libs/Smarty.class.php");
            $this->smarty = new Smarty();
            $this->smarty->compile_check = true;
            $this->smarty->caching = false;
            $this->smarty->cache_lifetime = 0;
     		$this->smarty->debugging = false;
			$this->smarty->template_dir = 'templates/';
			$this->smarty->compile_dir = 'compiled/';
			$this->smarty->config_dir = 'configs/';
			$this->smarty->cache_dir = 'cache/';

            $this->db=new Database($this->config->dbname,$this->config->dbhost,
    							$this->config->dbuser,$this->config->dbpass);
    		$this->db->connect();
    		$this->db->query("SET NAMES utf8");

   		    $query = 'SELECT * FROM settings';
    		$this->db->query($query);
    		$sts = $this->db->results();

    		foreach($sts as $s)
    		{
      			$name = $s->name;
    			$this->settings->$name = $s->value;
	   		}

			// Определяем корневую директорию сайта
			$this->root_dir = rtrim(dirname(dirname(($_SERVER["PHP_SELF"]))), '/');
			$this->smarty->assign('root_dir', $this->root_dir);

			// Корневой url сайта
			$dir = trim(dirname(dirname($_SERVER['SCRIPT_NAME'])));
			$dir = str_replace("\\", '/', $dir);
			$this->root_url = $_SERVER['HTTP_HOST'];

            if ($this->config->env=='local'){
                $this->root_url .= ":" . $this->config->port;
            }

            $this->root_admin_url = 'http://' . $this->root_url . dirname($_SERVER['SCRIPT_NAME']);

            //if ($this->config->env=='dev'){
            //    $this->root_admin_url .= ":" . $this->config->port;
            //}

            if($dir!='/') {
                $this->root_url = $this->root_url . $dir;
            }
			$this->smarty->assign('root_url', $this->root_url);

    		// Если не установлена библиотека GD, не используем ее
            if(!extension_loaded('gd'))
              $this->use_gd = false;
              
			// Устанавливает token для защиты от xss
			if(empty($_GET) && empty($_POST))
			{
				$this->token =  md5(uniqid(rand(), true));
				$_SESSION['token'] = $this->token;
			}elseif(isset($_SESSION['token']) && !empty($_SESSION['token']))
			{
				$this->token = $_SESSION['token'];  			
			}else
			{
				$this->token = '';
			}
	   		$this->smarty->assign('Token', $this->token);

            $this->smarty->assign('UseGd', $this->use_gd);

	   		$this->smarty->assign('Settings', $this->settings);
	   		$this->smarty->assign('Config', $this->config);

            $this->setka = new Setka();
            $setkaConfig = $this->setka->setConfig();
            $this->smarty->assign('contentEditorCss', $this->setka->contentEditorCss);
            $this->smarty->assign('themeFileCss', $this->setka->themeFileCss);
            $this->smarty->assign('contentEditorJs', $this->setka->contentEditorJs);



            // проверим авторизацию пользователя.
            $this->user = null;

            if(isset($_SESSION['adminarea']['login']) && isset($_SESSION['adminarea']['user_password']) && $_SESSION['adminarea']['admin']===true)
            {
                $login = $_SESSION['adminarea']['login'];
                $password = $_SESSION['adminarea']['user_password'];
                if(!empty($login) && !empty($password))
                {
                    $query = sql_placeholder("SELECT * FROM users_admin WHERE login=? AND password=? AND enabled=1 LIMIT 1", $login, $password);
                    $this->db->query($query);
                    $this->user = $this->db->result();

                    // вытащим доступные для пользователя модули
                    $query = sql_placeholder('SELECT m.* FROM users_admin_permissions AS uap
                        INNER JOIN modules AS m ON m.module_id=uap.module_id
                        WHERE uap.user_id=? AND type_module = "admin"
                        ORDER BY m.order_num ASC
                        ', $this->user->id
                    );
                    $this->db->query($query);
                    $allowedModules = $this->db->results();

                    $this->smarty->assign('allowedModules', $allowedModules);
                    $this->smarty->assign('user', $this->user);
                }
            }

            if (!$this->user && $_SESSION['adminarea']['admin']===true) {
               // header('Location: '.$this->root_admin_url);
                exit();
            }

    	}
    }

    function fetch()
    {
    	$this->body="";
    }


    function param($name)
    {
    	if(!empty($name))
      	{
      		if(isset($this->params[$name]))
	  		  return $this->params[$name];
	  		elseif(isset($_GET[$name]))
	  		  return $_GET[$name];
    	}
	    return null;
    }

    function add_param($name)
    {
    	if(!empty($name) && isset($_GET[$name]))
    	{
			$this->params[$name] = $_GET[$name];
	        return true;
    	}
	    return false;
    }

    function form_get($extra_params)
    {
    	$copy=$this->params;
      	foreach($extra_params as $key=>$value)
      	{
	    	if(!is_null($value))
    	  	{
          		$copy[$key]=$value;
	        }
    	}

	    $get='';
    	foreach($copy as $key=>$value)
		{
        	if(strval($value)!="")
	        {
    		    if(empty($get))
            	  $get .= '?';
	        	else
    	          $get .= '&';
    	        $get .= urlencode($key).'='.urlencode($value);
        	}
	    }

      	return $get;
    }
    
    function email($to, $subject, $message)
    {
    	$site_name = "=?utf-8?B?".base64_encode($this->settings->site_name)."?=";
    	
    	if(!empty($this->settings->notify_from_email))
    		$from = "$site_name <".$this->settings->notify_from_email.">";
    	else
    		$from = "$site_name <simpla@".$_SERVER['HTTP_HOST'].">";
    		
    	$headers = "MIME-Version: 1.0\n" ;
    	$headers .= "Content-type: text/html; charset=utf-8; \r\n"; 
    	$headers .= "From: $from \r\n";

    	
    	$subject = "=?utf-8?B?".base64_encode($subject)."?="; 
    	@mail($to, $subject, $message, $headers);
    }    
    
    function check_token()
    {
    	$token = '';
    	if(isset($_GET['token'])) $token = $_GET['token'];
    	elseif(isset($_POST['token'])) $token = $_POST['token'];
    	if(empty($token)
    	|| !isset($_SESSION['token']) || empty($_SESSION['token'])
    	|| $token !== $_SESSION['token'])
    	{
    		header('Location: ' . $this->root_admin_url);
    		exit();
    	}
    }
    
    function image_resize($source_file, $dest_file, $max_width, $max_height)
    {
        if($this->use_gd){
            $old_image = imagecreatefromstring(file_get_contents($source_file));

            $new_width = $old_width = imageSX($old_image);
            $new_height = $old_height= imageSY($old_image);

            if($old_width > $max_width && $max_width>0){
                $new_height = $old_height * ($max_width/$old_width);
                $new_width = $max_width;
            }
            
            if($new_height > $max_height && $max_height>0){
                $new_width = $new_width * ($max_height/$new_height);
                $new_height = $max_height;
            }
            
            $new_image=ImageCreateTrueColor($new_width,$new_height);
            imageinterlace($new_image, true);
       
            imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);
   
            imagejpeg($new_image, $dest_file, $this->settings->image_quality);
        }
        elseif($source_file != $dest_file){
            copy($source_file, $dest_file);
        }
    }

    function im_image_resize($fullpath, $writeTo, $width, $height)
    {
        $thumb = new Imagick();
        //читаем картинку по полному пути
        $thumb->readImage($fullpath);

        //делаем превью, размер меньше, чем у фона, чтобы было куда впихнуть тень
//        $thumb->thumbnailImage($width, $height);
        $thumb->cropThumbnailImage($width, $height);


        //наводим резкость, если превью мелкое
        if ($width < 300){
            $thumb->sharpenImage(4, 1);
        }
         
        $thumb->writeImage($writeTo);
        //подчищаем за собой
        $thumb->destroy();
    }


    /**
     * удаление элемента
     *
     * @param $itemId
     */
    function deleteItem($itemId, $tableName=''){
        if ($tableName!=''){
            if (isset($itemId) && !empty($itemId)) {
                $items = array($itemId);
            }

            if (is_array($items)) {
                $items_sql = implode("', '", $items);
            } else {
                $items_sql = $items;
            }

            $query = "DELETE FROM {$tableName} WHERE id IN ('$items_sql')";
            $this->db->query($query);
        }

    }

    /**
     * Активация/Дезактивация элемента
     *
     * @param $itemId
     * @param string $tableName
     */
    function setEnable($itemId, $tableName=''){
        if ($tableName!=''){
            $query = sql_placeholder('UPDATE ' . $tableName . ' SET enabled=1-enabled WHERE id=?', $itemId);
            $this->db->query($query);
        }
    }

    function setShow($itemId, $tableName=''){
        if ($tableName!=''){
            $query = sql_placeholder('UPDATE ' . $tableName . ' SET isshow=1-isshow WHERE id=?', $itemId);
            $this->db->query($query);
        }
    }

    /**
     * транслит из кир=>лат
     *
     * @param $str
     * @return string
     */
    function translit($str)
    {
        $str = trim ($str);
        $tr = array(
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
            "Д"=>"d","Е"=>"e","Ё"=>"E","Ж"=>"j","З"=>"z","И"=>"i",
            "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
            "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
            "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
            "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
            "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            " "=> "-", "."=> "", "/"=> "-", "»"=>"", "«"=>"",
            " - "=>"-", " — "=>"-", "&quot;"=>"", "!"=>"", "”"=>"",
            "“"=>"", ","=>"", "("=>"", ")"=>"", "°"=>"", "\""=>"",
            "№"=>"", "*"=>"-", "\""=>"", "'"=>"", "+"=>"", "%"=>"",
            ":"=>"", "&"=>"", "–"=>"", "`"=>""
        );
        return strtr($str,$tr);
    }

}
