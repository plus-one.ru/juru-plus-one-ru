<?PHP
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
// Засекаем время
//if ($_POST){
//    print_r($_POST);
//    print_r($_FILES);
//    die();
//}


$time_start = microtime(true);

require_once('Page.admin.php');
session_start();

// Кеширование в админке нам не нужно
Header("Expires:Mon, 26 Jul 1997 05:00:00 GMT");
Header("Last-Modified:".gmdate("D,d M Y H:i:s"));
Header("Cache-Control:no-cache,must-revalidate");
Header("Pragma: no-cache");

// Если в админку перешли неизвестно откуда, просто показать главную страничку
//if(empty($_SERVER['HTTP_REFERER']) || !preg_match('#^http://'.$_SERVER['HTTP_HOST'].'#i',$_SERVER['HTTP_REFERER']) && (!empty($_GET) || !empty($_POST)))
//{
//  //header('Location: ./');
//  //exit();
//}

// Page ни от кого не наследуется так что передаем ноль
$page = new Page($a = 0);

$page->fetch();
$page->db->disconnect();

// Выводим страницу на экран
print $page->body;

?>
