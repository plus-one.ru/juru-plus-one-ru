<?PHP

require_once('Widget.admin.php');

// Этот класс выбирает модуль в зависимости от параметра Section и выводит его на экран
class Page extends Widget
{
    // Модуль (пока неизвестно какой)
	var $module;
    var $section;

//    var $allowed_modules = array(
//        "MainPage",
//        "BlogTag", "BlogTags",
//        "BlogPosts", "BlogPost",
//        "BlogWriters", "BlogWriter",
//        "BlogPartners", "BlogPartner",
//        "TypeMaterials", "TypeMaterial",
//        "PostTags", "PostTag",
//        "UnitMeasures", "UnitMeasure",
//        "Permissions", "Permission",
//        "Login"
//    );
    var $allowed_modules = array(
        "MainPage",
        "Login"
    );

	
	// Конструктор
	function Page(&$parent)
	{
	    // Вызываем конструктор базового класса
		parent::Widget($parent);
		
		$this->add_param('section');

        // проверяем - залогинен ли пользователь в админке
        if ($_SESSION['adminarea']['admin']===true){

            // кто к нам пришел
            $userLogin = $_SESSION['adminarea']['login'];
            $userPass = $_SESSION['adminarea']['user_password'];

            $query = sql_placeholder('SELECT * FROM users_admin WHERE login=? AND password=?', $userLogin, $userPass);
            $this->db->query($query);
            $user = $this->db->result();

            if ($user){
                // смотрим на разрешенные нам модули
                $query = sql_placeholder('SELECT * FROM users_admin_permissions AS uap
                        INNER JOIN modules AS m ON m.module_id=uap.module_id
                        WHERE uap.user_id=?', $user->id);
                $this->db->query($query);
                $allowedModules = $this->db->results();

                foreach ($allowedModules AS $allowedModule){
                    array_push($this->allowed_modules, $allowedModule->class);
                    array_push($this->allowed_modules, $allowedModule->class . "s");
                }
            }
            else{
                // если не нашли такого юзера - редиректим на авторизацию
                $section = 'Login';
                // Подключаем файл с необходимым модулем
                require_once($section.'.admin.php');

                // Создаем соответствующий модуль
                if(class_exists($section)) {
                    $this->module = new $section($this);
                    $this->section = $section;
                }
            }


            // Берем название модуля из get-запроса
            $section = $this->param('section');

            // Если запросили недопустимый модуль - используем модуль MainPage
            if(empty($section) || !in_array($section, $this->allowed_modules)) {
                $section = 'MainPage';
            }

            // Подключаем файл с необходимым модулем
            require_once($section.'.admin.php');

            // Создаем соответствующий модуль
            if(class_exists($section)) {
                $this->module = new $section($this);
                $this->section = $section;
            }
        }
        else{
            // Берем название модуля из get-запроса
            $section = 'Login';

            // Подключаем файл с необходимым модулем
            require_once($section.'.admin.php');

            // Создаем соответствующий модуль
            if(class_exists($section)) {
                $this->module = new $section($this);
                $this->section = $section;
            }

        }



	}

	function fetch()
	{
		$this->module->fetch();
		$this->smarty->assign("Title", $this->module->title);
		$this->smarty->assign("Keywords", $this->module->keywords);
		$this->smarty->assign("Description", $this->module->description);
		$this->smarty->assign("Body", $this->module->body);

        if ($this->section=="Login" && empty($_GET['action'])){
            $this->body = $this->smarty->fetch('index_login.tpl');
        }
        elseif($this->section=="Login" && $_GET['action'] == "changepassword"){
            $this->body = $this->smarty->fetch('index.tpl');
        }
        else{
            $this->body = $this->smarty->fetch('index.tpl');
        }

	}
}