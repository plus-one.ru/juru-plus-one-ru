{literal}
    <style>
        #dataTable_filter label{width:100%}
        #dataTable_filter label input {width:86%}
        th {font-weight: 500; color: #000}
    </style>
{/literal}

<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            <small>{$title}</small>
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>



<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                &nbsp;
                <div class="pull-right">
                    <div class="btn-group">
                        <a class="btn btn-primary btn-xs" type="button" href="blogpost/add/post/{$Token}" >
                            Новый номинант
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table id="dataTable" class="table order-column">
                    <thead>
                    <tr>
                        <th style="width:2%;"></th>
                        <th style="width:10%; cursor: pointer">Создан</th>
                        <th style="width:10%; cursor: pointer">Изменен</th>
                        <th style="width: 35%; cursor: pointer">Название</th>
                        <th style="width: 35%; cursor: pointer">Организация</th>
                        <th style="width: 35%; cursor: pointer">Номинация</th>
                        <th style="width: 35%; cursor: pointer">Средний балл</th>

                        <th></th>
                    </tr>
                    </thead>
                </table>
                <div class="row">
                    <div class="col-lg-12">
                        <p align="center">{$PagesNavigation}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>