<ul class="nav" id="side-menu">
    <li>
        <a href="./?r=s"><i class="fa fa-dashboard fa-fw"></i> Главная</a>
    </li>
    {*<li>*}
        {*<a href="#"><i class="fa fa-sticky-note-o fa-fw"></i> Контент<span class="fa arrow"></span></a>*}
        {*<ul class="nav nav-second-level">*}
            {*<li><a href="blogpost/">Записи в блогах</a></li>*}
            {*<li><a href="index.php?section=BlogTags">Теги (рубрики)</a></li>*}
            {*<li><a href="index.php?section=PostTags">Теги второго уровня</a></li>*}
            {*<li><a href="index.php?section=TypeMaterials">Тип материала</a></li>*}
        {*</ul>*}
    {*</li>*}
    {foreach item=module from=$allowedModules name=module}
            <li>
                <a href="index.php?section={$module->class|escape}s">
                    {if $module->icon != '0'}
                    <i class="fa {$module->icon|escape} fa-fw"></i> {$module->name|escape}
                    {else}
                    <i class="fa fa-sticky-note-o fa-fw"></i> {$module->name|escape}
                    {/if}
                </a>
            </li>
    {/foreach}
    {*<li>*}
        {*<a href="index.php?section=BlogWriters">*}
            {*<i class="fa fa-sticky-note-o fa-fw"></i> Авторы*}
        {*</a>*}
    {*</li>*}
    {*<li>*}
        {*<a href="index.php?section=BlogPartners">*}
            {*<i class="fa fa-sticky-note-o fa-fw"></i> Партнеры*}
        {*</a>*}
    {*</li>*}
    {*<li>*}
        {*<a href="index.php?section=UnitMeasures">*}
            {*<i class="fa fa-sticky-note-o fa-fw"></i> Единицы измерения*}
        {*</a>*}
    {*</li>*}
    {*<li>*}
        {*<a href="index.php?section=Permissions">*}
            {*<i class="fa fa-sticky-note-o fa-fw"></i> Пользователи*}
        {*</a>*}
    {*</li>*}
</ul>
