<label>Теги статьи</label>

<h5>Первого уровня</h5>
<select id="post_tag_first" class="selectpicker form-control" data-max-options="1" multiple data-live-search="true" name="post_tag_first" title="-- Укажите тег статьи для главной страницы --">
{foreach item=tag from=$tags name=tag}
    {if $tag->url != 'main'}
        <option value="{$tag->id}" {if $tag->id == $Item->tags}selected{/if}>{$tag->name|escape}</option>
    {/if}
{/foreach}
</select>

<h5>Второго уровня</h5>
<select id="post_tag_second" class="selectpicker form-control" data-max-options="1" multiple data-live-search="true" name="post_tag_second" title="-- Укажите тег статьи для главной страницы --">
    {foreach item=tag from=$blogTags name=tag}
        {if $tag->items}
            <optgroup label="{$tag->name}">
                {foreach item=tagItem from=$tag->items name=tagItem}
                    <option value="{$tagItem->id}" {if $tagItem->id == $Item->post_tag}selected{/if}>{$tagItem->name}</option>
                {/foreach}
            </optgroup>
        {/if}
    {/foreach}
</select>

<h5>Третьего уровня</h5>
<select id="post_tag_third" class="selectpicker form-control" name="post_tag_third" title="-- Выберите тип материала --">
{foreach item=typeMaterial from=$typeMaterials name=typeMaterial}
    <option value="{$typeMaterial->id}" {if $Item->type_material==$typeMaterial->id}selected{/if}>{$typeMaterial->name|escape}</option>
{/foreach}
</select>
