<ul class="list-inline">
    {foreach item=postTag from=$availableTags name=postTag}
        <li>
            {$postTag->name|escape}
        </li>
    {/foreach}
</ul>