<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

{if $Error}
    <div class="row">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {$Error}
        </div>
    </div>
{/if}

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    &nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=Specprojects&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group {if !$Item->header}has-warning{/if}">
                                        <label>Название спецпроекта</label>
                                        <input name="name" type="text" value='{$Item->name|escape}'
                                               placeholder="Укажите название спецпроекта" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Подзаголовок</label>
                                        <textarea rows="20" id="header" name="header" class="form-control smalleditor">{$Item->header}</textarea>
                                    </div>

                                    <div class="form-group {if !$Item->url}has-warning{/if}">
                                        <label>Цвет (для маркеров)</label>
                                        <input name="color" type="text" maxlength="6" value='{$Item->color|escape}'
                                               placeholder="Укажите цвет (для маркеров)" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Цвет (для текста)</label>
                                        <input name="font_color" type="text" maxlength="6" value='{$Item->font_color|escape}'
                                               placeholder="Укажите цвет (для текста)" class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->url}has-warning{/if}">
                                        <label>Url спецпроекта</label>
                                        <input name="external_url" type="text" value='{$Item->external_url|escape}'
                                               placeholder="Укажите Url спецпроекта" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="well">
                                    <label {if !$Item->tag}class="text-danger"{/if}>Анонс</label>

                                    <div class="form-group">
                                        {foreach item=tag from=$tags name=tag}
                                            {if $tag->url != 'main'}
                                                <label class="checkbox-inline">
                                                    <input type="radio" name="tags" value="{$tag->id}" {if $tag->id == $Item->tag}checked{/if}> {$tag->name|escape}
                                                </label>
                                            {/if}
                                        {/foreach}

                                    </div>
                                </div>


                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            {if $Item->image_1_1}
                                                <img id="large_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->image_1_1}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: 92%;"/>
                                                <p>
                                                    <a id="button_delete_largeimage"
                                                       class="btn btn-outline btn-danger btn-xs" type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="large_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Основное изображение</label>
                                                <input name="large_image" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_large_image" name="delete_large_image"/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</form>

{include file='tinymce_init.tpl'}