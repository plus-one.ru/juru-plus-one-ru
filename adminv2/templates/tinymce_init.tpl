
{*<script language="javascript" type="text/javascript" src="js/tiny_mce/plugins/smimage/smplugins.js"></script>*}
<script language="javascript" type="text/javascript" src="js/tinymce/tinymce.js"></script>

<script language="javascript">
	tinymce.init({literal}{{/literal}
		selector: '.smalleditor',
		height: 100,
		theme: 'modern',
        menubar: false,
        {*forced_root_block : 'div',*}
        {*forced_root_block_attrs: {literal}{{/literal}*}
            {*'class': 'text-block',*}
        {*{literal}}{/literal},*}
		plugins: [
		'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		'searchreplace visualblocks visualchars code fullscreen',
		'insertdatetime media nonbreaking save table contextmenu directionality',
		'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		],
		toolbar1: 'undo redo | insert | bold italic | code',
		//toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | code',
		image_advtab: false,
		templates: [
                {literal}{{/literal} title: 'Test template 1', content: 'Test 1' {literal}}{/literal},
                {literal}{{/literal} title: 'Test template 2', content: 'Test 2' {literal}}{/literal}
		],
		content_css: ["http://{$root_url}/design/{$Settings->theme}/css/main.css"],
        file_browser_callback :
            function(field_name, url, type, win){literal}{{/literal}

                var filebrowser = "filebrowser.php";
                filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;
                tinymce.activeEditor.windowManager.open({literal}{{/literal}
                    title : "Выбор изображения",
                    width : 800,
                    height : 600,
                    url : filebrowser
                    {literal}}{/literal}, {literal}{{/literal}
                    window : win,
                    input : field_name
                    {literal}}{/literal});
                    return false;
                {literal}}{/literal}
{literal}
	}
{/literal});



            tinymce.init({literal}{{/literal}
                        selector: '.fulleditor',
                        height: 500,
                        theme: 'modern',
                        menubar: false,
                        relative_urls: false,
                        valid_children: '+a[div|p|h1|h2|h3|h4|h5|h6|span]',
                        cleanup_on_startup: false,
                        trim_span_elements: false,
                        verify_html: false,
                        convert_urls: false,
                        cleanup: false,
                        {*forced_root_block : 'div',*}
                        {*forced_root_block_attrs: {literal}{{/literal}*}
                        {*'class': 'text-block',*}
                        {*{literal}}{/literal},*}
                        plugins: [
                            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                            'searchreplace visualblocks visualchars code fullscreen',
                            'insertdatetime media nonbreaking save table contextmenu directionality',
                            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc bulletImage imageVert imageGallery signLine headerTwoLevel citateNew dialogBox textColoredImg'
                        ],
                        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | image bulletImage imageVert imageGallery | signLine | headerTwoLevel | citateNew | dialogBox | textColoredImg',
                        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | code',
                        image_advtab: false,
                        templates: [
                                {literal}{{/literal} title: 'Подпись автора', content: '<span class="author">Инервью: Имя Автора, «+1»</span>' {literal}}{/literal},
                        ],
                        content_css: ["http://{$root_url}/design/{$Settings->theme}/css/main.css"],
                        file_browser_callback :
                                function(field_name, url, type, win){literal}{{/literal}

                                    var filebrowser = "filebrowser.php";
                                    filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;
                                    tinymce.activeEditor.windowManager.open({literal}{{/literal}
                                        title : "Выбор изображения",
                                        width : 800,
                                        height : 600,
                                        url : filebrowser
                                        {literal}}{/literal}, {literal}{{/literal}
                                        window : win,
                                        input : field_name
                                        {literal}}{/literal});
                                    return false;
                                    {literal}}{/literal}
                        {literal}
                    }
                    {/literal});

</script>
