<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="text-danger">{$Error}&nbsp;</span>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        {foreach item=comment from=$commentList name=comment}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="well">
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Имя судьи: <strong>{$comment->name|escape}</strong></label>
                                    </div>

                                    <div class="form-group {if !$Item->url}has-warning{/if}">
                                        <label>Дата: <strong>{$comment->date_created}<strong></label>
                                    </div>

                                    <div class="form-group {if !$Item->url}has-warning{/if}">
                                        <label>Комментарий:</label>
                                        <p>{$comment->comment|nl2br}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
