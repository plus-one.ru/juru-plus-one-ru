<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="text-danger">{$Error}&nbsp;</span>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=Judges&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="well">
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Имя судьи</label>
                                        <input name="name" type="text" value='{$Item->name|escape}'
                                               placeholder="Имя судьи" class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->url}has-warning{/if}">
                                        <label>Login (E-mail)</label>
                                        <input name="login" type="text" value='{$Item->login|escape}'
                                               placeholder="Укажите Login (E-mail)" class="form-control">
                                    </div>

                                    <div class="form-group {if !$Item->url}has-warning{/if}">
                                        <label>Пароль</label>
                                        <input name="password" type="password" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Активизировать
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>