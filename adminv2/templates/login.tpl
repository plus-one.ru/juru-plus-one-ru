
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Авторизация</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="index.php?section=Login">
                            <fieldset>
                                <div class="form-group">
                                  <p class="text-danger">{$error_login}</p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="логин" name="login" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="пароль" name="password" type="password" value="">
                                </div>
                                <button type="submit" class="btn btn-lg btn-success btn-block">Войти</button>
                            </fieldset>
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </div>