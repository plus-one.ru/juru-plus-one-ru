<?PHP
require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class Comment
 */
class Comment extends Widget
{
    var $item;
    private $salt = 'simpla';
    private $tableName = 'comments';

    function Comment(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $item_id = intval($this->param('item_id'));

        if (!empty($item_id)){
		    $query = sql_placeholder('SELECT * FROM blogposts WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
  	    }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Комментарий';
		}
		else{
			$this->title = 'Комментарий: ' . $this->item->name;
            $query = sql_placeholder("SELECT c.comment, u.name, u.login,
                                        DATE_FORMAT(FROM_UNIXTIME(c.created), '%d.%m.%Y %H:%i:%s') AS date_created
                                        FROM blogposts AS b
                                    INNER JOIN comments c on b.id = c.nominee_id
                                    INNER JOIN users u on c.judge_id = u.id
                                  WHERE b.id=?", $this->item->id);
            $this->db->query($query);
            $commentList = $this->db->results();
		}




        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
        $this->smarty->assign('commentList', $commentList);

		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);

		$this->body = $this->smarty->fetch('comment.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
//        echo $query;die();
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);

        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }
}
